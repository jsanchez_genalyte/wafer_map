function [ resp_sort_chip_table,sp_die_table,found_repl_flag ] = find_closest_nei( resp_sort_chip_table,sp_die_table,cros_ndx )
%FIND_CLOSEST_NEI Finds the closest neighboor for the first missing die.
% if there are no missing or no available: does NOTHING.
%   Uses the following rule: when equal distance goes up first. otherwhise uses closets. period.
found_repl_flag                  = false;
resp_sort_chip_table.sp_avn_flag = (~(resp_sort_chip_table.sp_use_flag)) & (resp_sort_chip_table.good_flag);
mis_ndx                          = find(resp_sort_chip_table.sp_mis_flag == 1); % these are the die_ndx that missed to be used for the sp due to failures.
ava_ndx                          = find(resp_sort_chip_table.sp_avn_flag == 1); % these are the die_ndx that avilable to be used for the sp: not requested but good
if ( (~(isempty(mis_ndx))) &&  (~(isempty(ava_ndx))) )
    % HAVE A MISSING AND HAVE AVAILABLE
    found_repl_flag = true;    
    die_mis_ndx     = mis_ndx(1); % process the first missing.
    dif_vec         = ava_ndx-die_mis_ndx;
    ava_back_flag   = dif_vec < 0;
    dif_vec_abs     = abs(dif_vec);
    [ d, ix ]       = nanmin( dif_vec_abs ); % d is how close they are. ix is the location of the min.
    % fprintf('Closest abs distance %-4d ndx = %-4d',d, ix(1));
    % work required to see if there are 2 closest neighboors: one backward and one forward
    [out,idx]=sort(dif_vec_abs);
    val_1 = out(1); % this is the min         distance from the available to the missing one.
    val_2 = out(2); % this is the second min  distance
    ndx_1 = idx(1); % this is the ndx of the min
    ndx_2 = idx(2); % this is the ndx of the second min
    % fprintf('\nClosest abs distance %-4d ndx = %-4d    Second Closest:  %-4d ndx = %-4d ',val_1,ndx_1,val_2,ndx_2);
    if (val_1 == val_2)
        % TWO CLOSEST NEIGHBOORS: Pick the backwards one
        if (ava_back_flag(ix) > 0)
            % MIN IS BACKWARD: Done with this neighboor.
            resp_sort_chip_table.sp_avn_flag(ava_ndx(ndx_1))= 0; % not avaliab anymore
            resp_sort_chip_table.sp_use_flag(ava_ndx(ndx_1))= 1; % now it is used
            resp_sort_chip_table.sp_mis_flag(die_mis_ndx)   = 0; % not missing anymore
            sp_die_table.sp_mis_flag(cros_ndx(die_mis_ndx)) = 0;
            
            
            resp_sort_chip_table.rpl_from_flag(die_mis_ndx) = 1;
            resp_sort_chip_table.rpl_with_ndx(die_mis_ndx)  = ava_ndx(ndx_1);
            %mis_ndx(cur_mis_ndx)                            = 0; % not missing anymore so we do not look for this again
            %ava_ndx(ndx_1)                                  = nan; % not avaliab anymore so we do not use this again.  ndx_1 should be same as ix
        else
            % MIN IS FORWARD: Get the forward, ie the second closest ? jas_tbr
            resp_sort_chip_table.sp_avn_flag(ava_ndx(ndx_2)) = 0; % not avaliab anymore
            resp_sort_chip_table.sp_use_flag(ava_ndx(ndx_2)) = 1; % now it is used
            resp_sort_chip_table.sp_mis_flag(die_mis_ndx)   = 0; % not missing anymore
            sp_die_table.sp_mis_flag(cros_ndx(die_mis_ndx)) = 0;
            resp_sort_chip_table.rpl_from_flag(die_mis_ndx) = 1;
            resp_sort_chip_table.rpl_with_ndx(die_mis_ndx)  = ava_ndx(ndx_2);
            %mis_ndx(cur_mis_ndx)                            = 0; % not missing anymore so we do not look for this again
            %ava_ndx(ndx_2)                                  = nan; % not avaliab anymore so we do not use this again.
        end
    else
        % ONE CLOSEST NEIGHBOORS: Pick it regardless of where it is:
        resp_sort_chip_table.sp_avn_flag(ava_ndx(ndx_1))    = 0; % not avaliab anymore: the neig
        resp_sort_chip_table.sp_use_flag(ava_ndx(ndx_1))    = 1; % now it is used:      the neigh
        resp_sort_chip_table.sp_mis_flag(die_mis_ndx)       = 0; % not missing anymore
        sp_die_table.sp_mis_flag(cros_ndx(die_mis_ndx))     = 0;
        % DID NOT GET IT SOME USE IS not GOOD AND WE did not COVER THE WHOLE SP(die_mis_ndx)   = 0; % not missing anymore
        resp_sort_chip_table.rpl_from_flag(die_mis_ndx)     = 1;
        resp_sort_chip_table.rpl_with_ndx(die_mis_ndx)      = ava_ndx(ndx_1);
        %mis_ndx(cur_mis_ndx)                               = 0; % not missing anymore
        %ava_ndx(ndx_1)                                     = 0; % not avaliab anymore
    end
end %  have a missing and have available
end % fn find_closest_nei

