function [ ] = wm_process_wafer( wafer_map_struct )
%WM_PROCESS_WAFER process and plot a wafer_map_struct
%   Detailed explanation goes here

% S1. Setup
%wm_wanted_header   = {'INDEX'	'ROW'	'COLUMN' 'FSR'};
%col_ndx_die = 1;
col_ndx_row = 2;
col_ndx_col = 3;
col_ndx_fsr = 4;

% S2. Fill in wafer_map_mat
die_all_cnt =0;
for cur_wafer_ndx = 1:1:size(wafer_map_struct,2)
    die_all_cnt = die_all_cnt+ size(wafer_map_struct(cur_wafer_ndx).data_mat,1);
    die_all_min = min(die_all_min,wafer_map_struct(cur_wafer_ndx).data_mat(:,1));
end

wafer_map_mat = nan(die_all_cnt,4);
for cur_wafer_ndx = 1:1:size(wafer_map_struct,2)
    wafer_map_mat(1:size(wafer_map_struct(cur_wafer_ndx).data_mat(:,4)),1:4) =    wafer_map_struct(cur_wafer_ndx).data_mat(:,1:4);
end
min_wafer = min(wafer_map_mat);
max_wafer = max(wafer_map_mat);
fprintf( '\n\t\t\tINDEX\t\t\t ROW    \t\tCOLUMN   \tFSR');
fprintf('\nMIN:= \t\t%-4d\t\t\t %-4d\t\t\t%-4d\t\t %-6.2f\t\t ',min_wafer(1),min_wafer(2),min_wafer(3),min_wafer(4));
fprintf('\nMAX:= \t\t%-4d\t\t\t %-4d\t\t\t%-4d\t\t %-6.2f\t\t ',max_wafer(1),max_wafer(2),max_wafer(3),max_wafer(4));
fprintf('\n');

% S4.Fill contour map
contour_data = nan(max_wafer(col_ndx_row),max_wafer(col_ndx_col));
for cur_die_ndx = 1:1:size(wafer_map_mat,1)
    cur_row = wafer_map_mat(cur_die_ndx,col_ndx_row);
    cur_col = wafer_map_mat(cur_die_ndx,col_ndx_col);
    cur_fsr = wafer_map_mat(cur_die_ndx,col_ndx_fsr);
    if (isfinite(cur_row) && isfinite(cur_col) && isfinite(cur_fsr) )
        contour_data(cur_row  ,cur_col )=  cur_fsr;
    end
end
%  figure
%  contourf(contour_data,20,'ShowText','on')

% S3. Set up Plot for  wafer_map_mat

plot_data_info_str.figure_title = sprintf('  WAFER MAP FROM CHUNG for dir: %-s ',lot_die_directory_name);
plot_data_info_str.plot_title   = lot_die_directory_name;
plot_data_info_str.xlabel_plot  = ' WAFER COL ';
plot_data_info_str.ylabel_plot  = ' WAFER ROW ';
plot_data_info_str.zlabel_plot  = ' FSR ';
wm_plot_scatter_3d( plot_data_info_str );

end % fn wm_process_wafer

