function [  ] = wm_plot_scatter_3d( plot_data_info_struct,wafer_map_mat )
%WM_PLOT_SCATTER_3D produces 3D plot: Z is FSR X,Y are wafer coordinates
%   The input struct must have the requiered fields.
% sample call:   [  ] = wm_plot_scatter_3d( plot_data_info_struct );

figure_title    = plot_data_info_struct.figure_title;
plot_title      = plot_data_info_struct.plot_title;
xlabel_plot    	= plot_data_info_struct.xlabel_plot;
ylabel_plot   	= plot_data_info_struct.ylabel_plot;
zlabel_plot  	= plot_data_info_struct.zlabel_plot;
% clean figure: _____________________________________________________________
close_all_true_figures;
fig_btm                = 50;
fig_left               = 50;
fig_width              = 1600;
fig_height             = 900;
hFig                   = figure('Name',figure_title);
set(hFig, 'Position', [fig_left fig_btm  fig_width fig_height]);

ax_box                  = gca;
scatter3(ax_box,wafer_map_mat(:,2),wafer_map_mat(:,3),wafer_map_mat(:,4)...
    ,'LineWidth'        ,0.2...
    ,'MarkerFaceColor'  ,'Red'...
    ,'MarkerEdgeColor'  ,'Red'...
    ,'Marker'           ,'s'...
    ,'SizeData',9); % ,'MarkerFaceColor',dark_green_color);
hold all;
set(ax_box,'FontSize',9);
str_title = strcat('  DIE From Dir: ', strrep(plot_title,'_','-'));
title(ax_box,strrep(str_title,'.',' '),'FontSize',11);
ylabel(ax_box, xlabel_plot);
xlabel(ax_box, ylabel_plot);
zlabel(ax_box, zlabel_plot);
set(ax_box,'XGrid','on');
set(ax_box,'YGrid','on');
grid on;
grid minor;
hold off;
end % fn wm_plot_3d

