function  [ input_params,error_message ]  = wm_part_bin_update(input_params)
%wm_PART_BIN_UPDATE updates the bin for the chips defined for a given sampling plan, a wmot lot and list of chips).
%  Version 3.0 Using 11th ... Mario's version (source version 1.0)      MAIN_INPUT / PROCESS / OUTPUT
%  NOTES: Missing items: Find from the previous steps the 244 serials  step==fsr_heat_min-10=3-   fsr_heat_max==?A230
%                        Find serials in current  steps that are not in previous: add ons to replace the missing/failures
%                        Find serials in previous steps that are not in current:  failures (what kind??? Alex)
%                        assemble list with correct order (243,244 in position 20 and 40)
%                        assemble a column: with pass /fail.
%                        add logic for finding nearest neighboor. (see backup_wm_Ver_200 dir)
%
% pipleine:  wm_part_bin_update  --> wm_read_master_dir -->
%                               -->  perform_wm -->
% From Ryan:
% Here's the working example for doing part bin updates to tag which chip goes on which sampling plan.
%
%   Loads either a single file or all all found files from a dir and saves them in a data struct with:
%   matrix of doubles for each files.
% Connect to run card:

%input_params.webservice_url         = 'http://10.0.2.226/runcard_test/soap?wsdl'; % test
%webservice_url                      = 'http://10.0.2.226/runcard/soap?wsdl';      % production

% debug_flag = false; % version 
% if (debug_flag)
%     input_params
% end

% get wafer number and wafer_lot_number:

if (~(isempty(strrep(input_params.wafer_map_all_die_dir,' ',''))))
    % USER REQUESTED A TRUE DIRECTORY
    [ file_found_list,error_message] = wm_read_master_dir(input_params.wafer_map_all_die_dir );
    if (~(isempty(error_message)))
        return;
    end
    % OK LOADING FILES WITHIN DIR. Now load each file
    file_load_cnt = size(file_found_list,1);
    wafer_set_data_cell = cell(file_load_cnt,1);
    for cur_file_ndx = 1:1:file_load_cnt
        cur_wafer_file = file_found_list{cur_file_ndx};
        [ wafer_number,wafer_lot_number_str,error_message]= get_wafer_number_from_file_name( cur_wafer_file);        
        if (~(isempty(error_message)))
            return;
        end
        [ wafer_map_all_mat ,error_message ]       	= wm_get_excel_range( cur_wafer_file,1);   
        if (~(isempty(error_message)))
            return;
        end      
    % store results for current file        
        wafer_set_data_struct                       = {};
        wafer_set_data_struct.file_name             = cur_wafer_file;
        wafer_set_data_struct.wafer_number          = wafer_number;
        wafer_set_data_struct.wafer_lot_number_str  = wafer_lot_number_str;
        wafer_set_data_struct.wafer_map_all_mat     = wafer_map_all_mat;          
        wafer_set_data_cell{cur_file_ndx}           = wafer_set_data_struct;
        
    end % for each file found
else
    % USER REQUESTED A single file
    wafer_set_data_cell = cell(1,1);    
    % Do the actual read of the single file. but before check the name and extrack lot and wafer number.
    [ wafer_number,wafer_lot_number_str,error_message ] = get_wafer_number_from_file_name( input_params.wafer_map_all_die_file );
    if (~(isempty(error_message)))
        return;
    end
    % Real Processing:    
    [ wafer_map_all_mat ,error_message ]            = wm_get_excel_range( input_params.wafer_map_all_die_file,1);
    if (~(isempty(error_message)))
        return;
    end
    % store results for single file
    wafer_set_data_struct                           = {};    
    wafer_set_data_struct(1).file_name              = input_params.wafer_map_all_die_file;
    wafer_set_data_struct(1).wafer_number           = wafer_number;
    wafer_set_data_struct(1).wafer_lot_number_str   = wafer_lot_number_str;
    wafer_set_data_struct(1).wafer_map_all_mat      = wafer_map_all_mat; 
    wafer_set_data_cell{1}                          = wafer_set_data_struct;    
end

% DONE LOADING EXCEL WAFER FILES.
% fields wafer_number_all_die,lot_number_all_str   
% wafer_map_all_die_dir = input_params.wafer_map_all_die_dir;
% wafer_map_all_die_file  = input_params.wafer_map_all_die_file;

% temporal to check green compile:

% fsr_heat_min              	= input_params.fsr_heat_min;
% fsr_heat_max             	= input_params.fsr_heat_max;
% webservice_url              = input_params.webservice_url;
% logical_str                 = {'FALSE', 'TRUE'};

wm_process_wafer( wafer_set_data_cell,input_params ); % input_params
end % fn wm_part_bin_update

% % % % noi: from old sp:
% % % wafer_map_number_to_use = double(wafer_map_number_to_use);
% % % if (wafer_map_cnt < wafer_map_number_to_use)
% % %     error_message = wmrintf(...
% % %         '\n ERROR: Invalid Sampling Plan Number: %4d. \n There are only %4d available sampling plans in the wafer_map_MASTER file .\n '...
% % %         ,wafer_map_number_to_use,wafer_map_cnt);
% % %     return;
% % % else
% % %     wafer_map_name_to_use       = wm_names_cel{wafer_map_number_to_use};
% % % end
% % % 
% % % 
