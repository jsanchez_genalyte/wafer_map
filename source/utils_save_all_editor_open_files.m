% UTILS_SAVE_ALL_EDITOR_OPEN_FILES  in the editor. Ignores the Untiled* files 
%
local_debug = 0;
file_struct=matlab.desktop.editor.getAll;
files_open_cnt =   length(file_struct);
for cur_file_ndx = 1:1:files_open_cnt;
    cur_file_name = file_struct(cur_file_ndx).Filename;
    if strcmpi('Untitled',cur_file_name(1:8))
        continue;
    end
    if file_struct(cur_file_ndx).Modified
        if (local_debug)
            display (sprintf('File     To Save = %s',cur_file_name));
        end
        % SAVE THE OPEN - UNSAVED FILE
        file_struct(cur_file_ndx).save;
        
    else
        if (local_debug)
            display (sprintf('File NOT To Save = %s',cur_file_name));
        end
    end
end

