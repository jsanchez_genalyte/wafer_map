close all;
clear;
clear obj;
clear all;


cache_dir                  = 'C:\Users\jsanchez\Documents\Temp\jsanchez\mcrCache9.1\';

% Check if the cfg folder exists:
if ( ( exist(cache_dir,'dir') == 7))
    % Folder Exist and it is a directory:  If dir exists: Remove all cache dirs
    cd  (cache_dir);
    % CMD for loop to recursivelly remove all dirs: To start clean and avoid compiling with the wrong runcard db.
    !for /d %G in ("sampl*") do rd /s /q "%~G"
end

% WE ARE CLEAN: Ready to compile: 

cd  ('C:\Users\jsanchez\Documents\wafer_map\source\');
utils_save_all_editor_open_files;

% to compile:
mcc     -m  wafer_map -a ..\configure    -o  wafer_map
%   mcc -m wafer_map  -a ..\configure  -o  wafer_map -R 'logfile','wafer_map.log'
% -a @runcard_wsdl
!copy wafer_map.exe C:\Users\jsanchez\Documents\wafer_map\exe\
!move wafer_map.exe \\genstore2\Users\Storage\jsanchez\Documents\wafer_map\exe\
% !C:\Users\jsanchez\Documents\wafer_map\exe\wafer_map.exe

% ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html
% h = matlab.desktop.editor.getAll; h.close
% cd 'C:\Users\jsanchez\Documents\algo2\source'
% cd 'C:\Users\jsanchez\Documents\fcc\source'
% cd 'C:\Users\jsanchez\Documents\wafer_map\source'

%   ~/Documents/Temp/jsanchez/mcrCache9.1/sampli1/sampling_pla/@runcard_wsdl

% Original character   Escaped character
% "                    &quot;
% '                    &apos;
% <                    &lt;
% >                    &gt;
% &                    &amp;



