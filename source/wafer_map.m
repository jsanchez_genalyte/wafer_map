% Main file: wafer_map.m
% Script to perform the wafer map given 2 excel files:
% 1 - config parameters file and
% 2 - Chung's excel file with all data for a wafer lot for a single wafer.
% fcc_app:
% %       h = matlab.desktop.editor.getAll; h.close
%     cd 'C:\Users\jsanchez\Documents\fcc\source\'
% to compile: see wm_build.


%   !copy wafer_map.exe C:\Users\jsanchez\Documents\wafer_map\exe\
%   !move wafer_map.exe \\genstore2\Users\Storage\jsanchez\Documents\wafer_map\exe\
%   ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html

% Define Arguments: (see end of this file)

% to test:  runcard_test: use wo: DWO-0051

% pipeline: wafer_map --> get_wm_input_params  --> wm_get_excel_input_params (excel)
%                         --> wm_part_bin_update   --> wm_read_wm_master_file    -->  wm_get_excel_range (excel)
%                                                  --> perform_wm                -->|
% Rev_history
% Rev 1.00 Date: 2018_06_21 clone from sampling plan source
% Rev 1.10 Date: 2018_06_28 first release to Mario and Alexa.
% Rev 1.11 Date: 2018_06_28 Added saving figures.
% Rev 1.2  Date: 2018_07_09 Added perifery die and uses wildcard API from Ryan

run_date = datestr(now);
fprintf('\n Initializing wafer map:   Rev 1.2  Production Reports 1306 Die \n Run Date-time                 %-s\n',run_date);
[ input_params,error_message ] = wm_get_input_params();
if (~(isempty(error_message)))
    fprintf('\n ERROR:\n%s\n',error_message);
    fprintf('\n');
    pause(20);
    return;
end
[ input_params,error_message ]  = wm_part_bin_update(input_params);

if ((~(isempty(error_message))))
    fprintf('\nERROR:,\n%s\n',error_message);
end
fprintf('\n\tANALYSIS OF wafer map DONE_________________________\n\n');
% _______________________________________________________________________________________________________
if (isdeployed)
    fprintf('\n To produce a TEXT report of this output:  \n');
    fprintf('\n    1. Copy to the clipboard all the output do: Edit_menu --> Select All --> Enter  ');
    fprintf('\n    2. Paste into an email message: CTRL-V to share the report with the wafer map Requester. \n');
    fprintf('\n To verify results: Follow these steps in RunCard:  \n');
    fprintf('\n   1. Talk with Mario... or ');
    fprintf('\n   2. In the terminal view select Inventory, filter for part number 29005 and a few other things ....jas_tbd');
    fprintf('\n\n Bye ... \n');
    % ALLOW THE USER TO DO CTRL-A CTRL-C
    pause(40);
end
% end script wafer_map