function [  input_params_struct, error_message  ] = ...
    wm_get_excel_input_params( an_excel_file_name,excel_tab_number)
%wm_GET_EXCEL_INPUT_PARAMS  returns a cell with the set input params:
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the wafer_map app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
%
% Output: a cell with 110 entries: in two columns:
% Assumptions:
%   1 - column one is the parameter name
%   2 - column 2 is the parameter value
%   3 - column 3 is the description: Ignored.
%   4 - the indices are in ascending order. and there are no dupplicated indices.error_message
error_message           = '';
input_params_struct     = struct;
input_params_cnt        = 8; % jas_jardcoded: number of expected input parameters in the excel file. Beyond that it will be ignored.
%input_params_names    	= cell(input_params_cnt,1);
%input_params_values   	= cell(input_params_cnt,1);           % up to excel_max_block_count analytes: names of data columns.

% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
cur_row             = 0;
row_found_cnt       = size(sheet_table,1); %
col_found_cnt       = size(sheet_table,2); %
if ( (col_found_cnt <2) ||  (row_found_cnt <input_params_cnt) )
    error_message = 'ERROR: Invalid Excel file format. Reading input parameters. Fix it and try again. \nExpect at least 2 cols and 4 rows!';
    return;
end

try
    input_params_names  = table2cell(sheet_table(2:input_params_cnt+1,1)); % first col is the  wm_names
    input_params_names  = strrep(input_params_names,'''','');
    input_params_values = table2cell(sheet_table(2:input_params_cnt+1,2)); % second col is the wm_values
    
    cfg_params_wanted_cel = { 'wafer_map_all_die_dir','wafer_map_all_die_file','fsr_heat_min','fsr_heat_max'...
        ,'webservice_url','wanted_contour_delta_fsr','wanted_contour_values_cnt_fsr','wafer_map_figure_output_dir'};
    %         ,'op_code','op_code_previous_step','bin_number_from','bin_number_to' ...
    %         ,'part_number' ...
    %         ,'wm_ok_do_partially_flag','replace_chip_if_miss_flag','check_availab_only_flag'};
    
    cfg_params_wanted_struct = struct(      ...
         'wafer_map_all_die_dir'    ,''     ...
        ,'wafer_map_all_die_file'   ,''     ...
        ,'fsr_heat_min'             ,''     ...
        ,'fsr_heat_max'             ,''     ...
        ,'webservice_url'           ,''     ...
        ,'wanted_contour_delta_fsr' ,''     ...
        ,'wanted_contour_values_cnt_fsr','' ...
        ,'wafer_map_figure_output_dir','');

    
    found_fields = isfield(cfg_params_wanted_struct, input_params_names);
    if (sum(found_fields) == input_params_cnt)
        % FOUND ALL THE NEEDED PARAMETERS: Assume they are in order.
        cur_row = 1;
        input_params_struct.wafer_map_all_die_dir           = input_params_values{1};
        cur_row = 2;
        input_params_struct.wafer_map_all_die_file          = input_params_values{2};
        cur_row = 3;
        input_params_struct.fsr_heat_min                    = str2double(input_params_values{3});
        cur_row = 4;
        input_params_struct.fsr_heat_max                    = str2double(input_params_values{4});
        cur_row = 5;
        input_params_struct.webservice_url                  = input_params_values{5};
        cur_row = 6;
        input_params_struct.wanted_contour_delta_fsr        = str2double(input_params_values{6});
        cur_row = 7;
        input_params_struct.wanted_contour_values_cnt_fsr	= str2double(input_params_values{7});
        cur_row = 8;
        input_params_struct.wafer_map_figure_output_dir   	= input_params_values{8};
                
        

    else
        error_message = 'ERROR: Invalid Excel file format. Fix it and try again:\n';
        fprintf('\n%s',error_message);
        fprintf('\n wafer_map_all_die_dir               = %-s',input_params_values{1});
        fprintf('\n wafer_map_all_die_file              = %-s',input_params_values{2});
        fprintf('\n fsr_heat_min                        = %-s',input_params_values{3});
        fprintf('\n fsr_heat_max                        = %-s',input_params_values{4});
        fprintf('\n webservice_url                      = %-s',input_params_values{5});
        fprintf('\n wanted_contour_delta_fsr            = %-s',input_params_values{6});        
        fprintf('\n wanted_contour_values_cnt_fsr    	= %-s',input_params_values{7});        
        fprintf('\n wafer_map_figure_output_dir       	= %-s',input_params_values{8});         
        return;
    end
catch
    error_message = sprintf('ERROR: Invalid Input Parameters Excel file format. \nFix it and try again. Error Reading row: %4d',cur_row);
    return;
end % catch

if   (      (isempty(strrep(input_params_values{1},' ',''))) ...
        && 	(isempty(strrep(input_params_values{2},' ',''))))
    error_message = sprintf('ERROR: Invalid Input Parameters Excel file format. \n     Need either a file or a directory \nFix it and try again. Error Reading row: %4d',cur_row);
    return;
end

fprintf('\n Configuration Parameters:     \n');
if (~(isempty(strrep(input_params_values{1},' ',''))))
fprintf('\n wafer_map_all_die_dir           = %-s',input_params_values{1});
end
if (isempty(strrep(input_params_values{1},' ','')))
fprintf('\n wafer_map_all_die_file (SINGLE) = %-s',input_params_values{2});
else
    fprintf('\n wafer_map_all_die_file   	= %-s will be ignored (Using Dir parameter)',input_params_values{2});
    input_params_struct.wafer_map_all_die_file ='';
end
fprintf('\n fsr_heat_min                    = %-s',input_params_values{3});
fprintf('\n fsr_heat_max                    = %-s',input_params_values{4});
fprintf('\n webservice_url                  = %-s',input_params_values{5});
fprintf('\n wanted_contour_delta_fsr     	= %-s',input_params_values{6});
fprintf('\n wanted_contour_values_cnt_fsr 	= %-s',input_params_values{7});
fprintf('\n wafer_map_figure_output_dir 	= %-s',input_params_values{8});


end % fn wm_get_excel_input_params

