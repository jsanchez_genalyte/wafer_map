function [ wafer_map_ext_mat,error_message ] = wm_runcard_get_bins_for_wafer(wafer_lot_number_str,wafer_number,wafer_map_mat,input_params)
%WM_RUNCARD_GET_BINS_FOR_WAFER gets from runcard the bins for a given wafer lot and wafer number.
%  Output: Adds a column to the wm_runcard_get_bins_for_wafer with the bin numbers.
%                                  P162143.23.0010
% set up loop invariants:
col_ndx_row             	= 2;    % excel deppendency: order of cols in excel file
col_ndx_col               	= 3;

%part_number                = '29005';  % Alex: part_number = 'EXP_CHIP'; %  Mario: '29005'
webservice_url              = input_params.webservice_url;
%logical_str                = {'FALSE', 'TRUE'};
% Connect to run card:

fprintf('\nRetrieving Bin data from RUNCARD. Please wait ~ 2 minutes per file .....\n');
createClassFromWsdl(webservice_url);
obj                        	= runcard_wsdl;                                                   %
% Real Processing:
wafer_die_cnt               = size(wafer_map_mat,1);
%wafer_bins                  = nan(wafer_die_cnt,1);
wafer_number_str            = sprintf('%2d',wafer_number);
wafer_number_str            = strrep(wafer_number_str,' ','0');
die_serial_part_1           = strcat('P',wafer_lot_number_str,'.',wafer_number_str,'.');



% get all serials from runcard using the wildcard:
wanted_serial_all           = strcat(die_serial_part_1,'*');
part_number                 = ''; % this will give them all: 1306 vs: 29005 or 'EXP_CHIP' or see eof with all current part numbers.
[resp_serial_all,error_value,error_message]   = fetchInventoryItems(obj, wanted_serial_all,'',part_number, '', '', '' ,  '', '', '');  % op_code=A
if (~(isempty(error_message)))
    error_message =  strcat( error_message, '\nRuncard fetchInventoryItems Error: = ',num2str(error_value), ' for lot: ',wafer_lot_number_str, ' and wafer ',num2str(wafer_number));
    return;
end;
total_die_from_runcard      = size(resp_serial_all,1);
die_no_meas_fsr_cnt         = total_die_from_runcard - wafer_die_cnt;
wafer_map_ext_mat           = nan(total_die_from_runcard,size(wafer_map_mat,2)+1);
resp_serial_all_table       = struct2table(resp_serial_all);
part_bin_all_str            = resp_serial_all_table.part_bin;
part_bin_all                = str2double(part_bin_all_str);

% old way: wafer_map_ext_mat(:,end)    = part_bin_all; % store all bins from runcard in last column of the ext matrix
chip_xy_ref = fetch_chip_xy_reference(); %fetch reference X/Y look up for chip index: This generates the map: 1306 x 2:  x and y i.e. col,row on the wafer.

% Reconcilitate the 2 lists: from excel and from runcard(master).
for cur_die_ndx =1:1:wafer_die_cnt
    % Fetch inventory for each serial:
    die_str                 = sprintf('%4d',wafer_map_mat(cur_die_ndx,1));  %  col 1 is the die number
    die_number_cur          = str2double(die_str);
    % HAVE THE NDX FROM the excel file: Store the current row of the mat into the correct row.   
    wafer_map_ext_mat(die_number_cur,1:end-1)   = wafer_map_mat(cur_die_ndx,1:end);
    wafer_map_ext_mat(die_number_cur,end)       = part_bin_all(die_number_cur);
end % for each die in the wafer

% Set the un-measured bin: I.E. All the ones that did not showed up on the excel file (still nans)
unmeas_ndx                                      = isnan(wafer_map_ext_mat(:,end));
wafer_map_ext_mat(unmeas_ndx,end)               = 7; % 7 is the category for the unmeasured die. 
wafer_map_ext_mat(unmeas_ndx,col_ndx_row)   	= chip_xy_ref(unmeas_ndx,2); % second col is the row
wafer_map_ext_mat(unmeas_ndx,col_ndx_col)       = chip_xy_ref(unmeas_ndx,1); % first  col is the col 
all_die_serial                                  = 1:1:total_die_from_runcard;
all_die_serial                                  = all_die_serial';
wafer_map_ext_mat(unmeas_ndx,1)                 = all_die_serial(unmeas_ndx,1); % fill in the  remainding die numbers (the perifery in gray).

% Fill in a fake ring just for the looks: 


% Report what you found from RunCard: 
fprintf('\n\n\t LOT:  \t\t\t%-s   \n\t WAFER: \t\t%-s RUNCARD DATA:',wafer_lot_number_str,num2str(wafer_number));
fprintf('\n\t Runcard      \t     total cnt = : %-4d\n', total_die_from_runcard);
fprintf('\n\t Measured     \tFSR  total cnt = : %-4d\n', wafer_die_cnt);
fprintf('\n\t Non-Measured \tFSR  total cnt = : %-4d\n', die_no_meas_fsr_cnt);
fprintf('\n');

end % fn sp_part_bin_update

% left overs (old way...)
% part_number = 29005    gives    96 that are available. (in inventory)
% part_number='EXP_CHIP' gives  1210
% sum:                          1306 

% for cur_die_ndx =1:1:wafer_die_cnt
%     % Fetch inventory for each serial:
%     die_str                 = sprintf('%4d',wafer_map_mat(cur_die_ndx,1));  %  col 1 is the die number
%     die_str                 = strrep(die_str,' ','0');                                % replace blanks with zeros.
%     wanted_serial           = strcat(die_serial_part_1,die_str);
%     [resp_serial, error_value, error_message]   = fetchInventoryItems(obj, wanted_serial,'',part_number, '', '', '' ,  '', '', '');  % op_code=A
%     if (~(isempty(error_message)))
%         error_message =  strcat( error_message, '\nRuncard fetchInventoryItems Error: = ',num2str(error_value), ' for lot: ',wafer_lot_number_str, ' and wafer ',num2str(wafer_number));
%         return;
%     end;
%     % HAVE THE SERIAL STRUCT FROM RUNCARD: Store the bin
%     if (~(isempty(resp_serial)))
%         wafer_bins(cur_die_ndx) = str2double(resp_serial.part_bin);
%     else
%         die_no_meas_fsr_cnt = die_no_meas_fsr_cnt+1;
%         %fprintf('\nEmpty runcard response for die: %s\n', die_str);
%     end
% end % for each die in the wafer

% if (cur_die_ndx == wafer_die_cnt)
%     fprintf('\n\n\t LOT:  \t\t%-s   \n\tWAFER: %-s COMPLETE RUNCARD DATA.',wafer_lot_number_str,num2str(wafer_number));
%     wafer_map_mat = [ wafer_map_mat wafer_bins];
% end

% current part numbers (from runcard)
% RUNCARD_BOM_MASTER PART_CLASS: XXXXXX 
% 00026	    
% SALT-CHIP	
% 00089		
% 00090				
% 00064		
% 00027		
% 00175		
% 00026
% 
% RUNCARD_BOM_MASTER PART_CLASS: CHIP_SPOTTED:
% EXP_CHIP_EXCL
% EXP_CHIP	
% 29005_EXT	
% 00445-EXCL	
% 00445		
% 00089		
