function [ file_found_list,error_message] = wm_read_master_dir(lot_die_directory_name)
%WM_READ_MASTER_DIR retrieves from the hard drive one or more files associated with a directory
%   Rules:
%   1. A falid wafer name must have to bubstrings: Lot and Wafer.
%   2. if one or more valid file names are found on the dir: Uses them.
%   3. if no files found: Go tru each subdir with name like: Wafer and find valid files within each sub dir
%                            O:\DIE TEST STATION\Lot 13 Revised FSRs
%   lot_die_directory_name ='O:\DIE TEST STATION\Lot 13 Revised FSRs'
%   lot_die_directory_name ='C:\Users\jsanchez\Documents\wafer_map\data\wl_161243\'      % with wafer files at the dir level
%   lot_die_directory_name ='C:\Users\jsanchez\Documents\wafer_map\data\test_dir\'    % with no wafer files, but with a wafer 23 dir to dig down.
%   sample call:
%   [ file_found_list,error_message] = wm_read_master_dir(lot_die_directory_name)

error_message           = '';
file_found_list         = {}; % delete('C:\Users\jsanchez\Documents\algo2\db_data_scan\*.mat')
wafer_file_found_cnt    = 0;

% CLEAN UP Dir name: if O: change it to the true name, so matlab knows how to deal with it. 
if ( (contains(lot_die_directory_name(1:5),'O:')) ||(contains(lot_die_directory_name(1:5),'o:')) )  % any of the first 5 chars is an O:
    lot_die_directory_name = strrep(lot_die_directory_name,'O:','\\gserver2.genalyte.local\share\Operations\');
    lot_die_directory_name = strrep(lot_die_directory_name,'o:','\\gserver2.genalyte.local\share\Operations\');
end


if (~(exist(lot_die_directory_name, 'dir') == 7))
    % DIR DOES NOT EXIST: return
    error_message   = sprintf('INPUT DATA DIRECTORY does not exist %s',lot_die_directory_name);
    return;
end

% get the list of files to see if there are any wanted files at this level. If none: go down one level for each sub dir.
lot_die_info            = dir(lot_die_directory_name);
lot_die_table           = struct2table(lot_die_info);
file_filt               = lot_die_table.isdir;
file_list               = lot_die_table.name(~file_filt);

file_found_list = cell(size(file_list,1),1);
% check if there are wafer files here
for cur_file_ndx=1:1:size(file_list,1)
    if ( contains(file_list(cur_file_ndx),'~$'))
        % OPEN FILE: skip it!
        continue;
    end
    if ( (contains(file_list(cur_file_ndx),'Lot')) && (contains(file_list(cur_file_ndx),'.xlsx')) && (contains(file_list(cur_file_ndx),'Wafermap All')) )
        wafer_file_found_cnt                    = wafer_file_found_cnt+1;
        file_found_list{wafer_file_found_cnt}   = strcat(lot_die_directory_name,'\',file_list{cur_file_ndx});
    end
end

if (wafer_file_found_cnt)
    % FOUND AT LEAST ONE FILE: we are done
    fprintf('\n Reading %-4d wafer_maps    ... DONE           ',wafer_file_found_cnt);    
    file_found_list                             = file_found_list(1:wafer_file_found_cnt);    
    return;
end

% NO FILES FOUND so FAR IN CURRENT DIR: Dig into the dirs with a name containing the word: Wafer
dir_filt            = lot_die_table.isdir;
dir_list            = lot_die_table.name(dir_filt);
dir_list            = dir_list(3:end); % get rid of . and ..
wafer_file_found_cnt= 0;
file_found_list     = cell(100,1);
% check if there are wafer files here
for cur_dir_ndx=1:1:size(dir_list,1)
    
    if (~(contains(dir_list{cur_dir_ndx},'Wafer')))
        % NON WAFER DIR: Ignoring this dir: it is not a wafer directory
        continue
    end
    % HAVE A WAFER DIR:
    lot_die_info    = dir(strcat(lot_die_directory_name,'\',dir_list{cur_dir_ndx}));
    lot_die_table   = struct2table(lot_die_info);
    file_filt       = lot_die_table.isdir;
    file_list       = lot_die_table.name(~file_filt);
    
    % check if there are wafer files here
    for cur_file_ndx=1:1:size(file_list,1)
        if ( contains(file_list(cur_file_ndx),'~$'))
            % OPEN FILE: skip it!
            continue;
        end
        if ( (contains(file_list(cur_file_ndx),'Lot')) && (contains(file_list(cur_file_ndx),'.xlsx'))  && (contains(file_list(cur_file_ndx),'Wafermap All')) )
            wafer_file_found_cnt            = wafer_file_found_cnt+1;
            file_found_list{wafer_file_found_cnt} =  strcat(lot_die_directory_name,'\',dir_list{cur_dir_ndx},'\',file_list{cur_file_ndx});
        end
    end % for each file within the dir
end % for each dir within the dir.

if (wafer_file_found_cnt)
    % FOUND AT LEAST ONE FILE: we are done
    fprintf('\n Reading %-4d wafer_maps    ... DONE           ',wafer_file_found_cnt);
    file_found_list = file_found_list(1:wafer_file_found_cnt);
    return;
else
    error_message =sprintf('ERROR: Input directory: %-s has no wafer files nor any of the subdirectories'...
        ,lot_die_directory_name);
end



end % fn wm_read_master_dir

% left overs:

% % % lot_die_info = dir(strcat(lot_die_directory_name,'\Dies FSR Actual Lot*.xlsx'));
% % %
% % % files_found_cnt = size(lot_die_info,1);
% % % if (files_found_cnt > 0)
% % %     % DIR DOES NOT EXIST: return
% % %     info_message   = sprintf('INPUT DATA DIRECTORY has  %-4d WAFER DIE files',files_found_cnt);
% % %     disp(info_message);
% % %     return;
% % % end
% % % % FILE FOUND OK: Get DIE FSR from the files
% % % %lot_die_data_struct  	= struct(files_found_cnt,1); % delete('C:\Users\jsanchez\Documents\algo2\db_data_scan\*.mat')
% % %
% % % for cur_file_ndx = 1:1:files_found_cnt
% % %     full_file_name              = strcat(lot_die_directory_name,'\',lot_die_info(cur_file_ndx).name);
% % %
% % %     [ data_mat ,error_message ] = wm_get_excel_range( full_file_name,1);
% % %     if ((isempty(error_message)) || (strcmp(error_message(1:7),'WARNING')) )
% % %         %fprintf('\n DONE  ... reading wafer_maps: \t   %-3d wafer_maps\n',size(wafer_map,2));
% % %         lot_die_data_struct(cur_file_ndx).data_mat  = data_mat;
% % %         lot_die_data_struct(cur_file_ndx).file_name = lot_die_info(cur_file_ndx).name;
% % %         % other info here to come jas_tbd.
% % %     else
% % %         % Error reading the LOT DIE FILE
% % %         disp('\nerror')
% % %         %return;
% % %     end
% % % end % for each file found
% % %




% % sensor_scan_struct            = load (db_data_expe_scan_file_name,'-mat');
% %
% % if (isempty(sensor_scan_struct))
% %     error_message           = sprintf('Could not load file: %s',lot_die_directory_name.db_data_expe_scan_file_name);
% %
% %     return;
% % end
% % %niu: sensor_cnt      = sensor_scan_struct.sensor_cnt;
% % sensor_type     = sensor_scan_struct.sensor_type;            % 2 or 3 are analysis sensors: 2 is tr? and 3 is analytes?
% % proid           = sensor_scan_struct.proid;
% % % cvt: sensor_scan_struct into 3 fields:
% %
% % % lot_die_data_struct is an easy to swallow struct with 136 row arrays.
% % lot_die_data_struct = {};
% % lot_die_data_struct.sensor_cnt     = sensor_scan_struct.sensor_cnt;
% % lot_die_data_struct.scan_len_max   = sensor_scan_struct.scan_len_max;
% % lot_die_data_struct.scan_col_max   = sensor_scan_struct.scan_col_max;
% % lot_die_data_struct.sensor_type    = sensor_type;
% % lot_die_data_struct.proid          = proid;
% % %
% % % if ( ((sum(sum(isnan(sensor_scan_struct.sensor_data_mat)))) == lot_die_data_struct.sensor_cnt) || ( sensor_scan_struct.scan_len_max < 3))
% % %     % EITHER ALL NANS or NO SCAN DATA:
% % %     lot_die_data_struct.scan_data_time = nan;
% % %     lot_die_data_struct.scan_data_rgru = nan;
% % %     lot_die_data_struct.scan_data_ngru = nan;
% % % else
% % %     % DATA SEEMS TO BE OK:
% % %
% % %     lot_die_data_struct.scan_data_time = reshape(sensor_scan_struct.sensor_data_mat(:, 1),lot_die_data_struct.scan_len_max , []); % col 1 is time
% % %     lot_die_data_struct.scan_data_rgru = reshape(sensor_scan_struct.sensor_data_mat(:, 2),lot_die_data_struct.scan_len_max , []); % col 2 is rgru
% % %     lot_die_data_struct.scan_data_ngru = reshape(sensor_scan_struct.sensor_data_mat(:, 2),lot_die_data_struct.scan_len_max , []); % col 42 is ngru (2018_05_01: Not sure where this comes from : col 42 is ngru!!!)
% % %     %                                                                             % jas_tb_fix: changed 42 with 2   2018_05_01
% % %     lot_die_data_struct.scan_data_time = lot_die_data_struct.scan_data_time';
% % %     lot_die_data_struct.scan_data_rgru = lot_die_data_struct.scan_data_rgru';
% % %     lot_die_data_struct.scan_data_ngru = lot_die_data_struct.scan_data_ngru';
% % % end
% % % if (lot_die_directory_name.local_hd_save_scan_excel_flag)
% % %     % SCAN_OPTICAL_EXCEL:  user requested saving this exp: store the whole scan matrix
% % %     lot_die_data_struct.sensor_data_mat = sensor_scan_struct.sensor_data_mat; %jas_tbd: Save FSR and other cols of interest same way as raw gru
% % % end
% % %lot_die_data_struct.scan_data_ngru = lot_die_data_struct.scan_data_ngru'; % jas_temp_maybe this is not normalized gru!!!.
% % % niu: jas_dbg_only
% % % % for cur_sensor_ndx =1:1:sensor_scan_struct.sensor_cnt
% % % %     if(         ( lot_die_data_struct.scan_data_time(cur_sensor_ndx,1) ==  lot_die_data_struct.scan_data_time(cur_sensor_ndx,lot_die_data_struct.scan_len_max)) ...
% % % %             &&  ( lot_die_data_struct.scan_data_rgru(cur_sensor_ndx,1) ==  lot_die_data_struct.scan_data_rgru(cur_sensor_ndx,lot_die_data_struct.scan_len_max)))
% % % %         fprintf('\nERROR IN LOGIC FIRST AND LAST TIME-GRU VALUES MATCH \n');
% % % %     end
% % % % end
% % % exp_scan_sensors            = nan(sensor_cnt,scan_sensors_col_dim); niu
% %
% % % niu: ? tbv
% % % scan_data_plane_time       = 1;
% % % scan_data_plane_gru        = 2;
% % % scan_data_plane_dim         = 2;
% exp_scan_data               = nan(sensor_cnt,max_scan_esti_len,scan_data_plane_dim );

%     sensor_data_mat(cur_start_row:cur_end_row,2:size(cur_sensor_data,2)+1) = cur_sensor_data;
%     sensor_data_time(cur_start_row:cur_end_row,1:size(cur_sensor_data,2))  = cur_sensor_data(:,1;

% structs:
%  stored in files:  3 integers: sensor_cnt,max_scan_len,max_scan_col,
%                    1 matrix:   scan_data_matrix   dim(sensor_cnt*max_scan_len,max_scan_col)
%  recovered    sensor_data_time   scan_data_matrix(:,1)
%  recovered    sensor_data_grur   scan_data_matrix(:,2)
%  build        sensor_data_numb   1:1:sensor_cnt          replicated
%  build        sensor_data_len    max_scan_len            replicated