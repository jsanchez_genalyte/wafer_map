function [ lot_die_data_mat, error_message  ] = ...
    wm_get_excel_range( an_excel_file_name,excel_tab_number)
%wm_GET_EXCEL_RANGE returns a matrix with the data found in the excel file.
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the std_curve_fit app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
% Output: matrix with data and empty error msg if no errors.
% Assumptions:
%   1 - There is one or more cells with this key: "wafer_map_xxx"
%   2 - There is data for at least 1 wm in the file.
%   3 - the second row wmecifies the length of the wm
%   4 - the indices are in ascending order. and there are no dupplicated indices.
% pipeline:  wafer_map  -->  wm_part_bin_update --> wm_read_wm_master_file --> wm_get_excel_range
error_message      = '';
lot_die_data_mat   = [];
%wm_die_max_cnt    = 200; % jas_jardcoded: 1000 wafer_maps
wm_col_max_cnt     = 4;
%wm_names_cel      = cell(wm_die_max_cnt,1);           % up to excel_max_block_count analytes: names of data columns.

if (contains(an_excel_file_name,'All','IgnoreCase',true))
    % PROCESSING ALL die file: Use proper header
    wm_wanted_header   = {'DIE INDEX'	'ROW'	'COLUMN' 'FSR'};                        % **** EXCEL DEPPENDENCY **** Not it is an argument to this function
else
    % PROCESSING ALL die file: Use proper header
    wm_wanted_header   = {'INDEX'	'ROW'	'COLUMN' 'FSR'};                        % **** EXCEL DEPPENDENCY **** Not it is an argument to this function
    
end
% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );

wm_die_cnt          = size(sheet_table,1)-1; %
wm_row_cnt          = size(sheet_table,2);  %
if ((wm_die_cnt <1) ||( wm_row_cnt ~=wm_col_max_cnt))
    error_message   = 'ERROR: Invalid Excel file format. Reading wafer_maps. Fix it and try again';
    return;
end
wm_names_cel        = table2cell(sheet_table(1,1:4)); % first              row  is  the  header wm_names
for cur_col_ndx     = 1:1:wm_col_max_cnt
    if (strcmp(wm_wanted_header{cur_col_ndx},upper(wm_names_cel{cur_col_ndx})))
        continue;
    else
        error_message = sprintf('ERROR: Invalid Excel file format. Reading file header. \n Expected: %s Found: %s'...
            ,wm_wanted_header{cur_col_ndx},wm_names_cel{cur_col_ndx});
        return;
    end
end
% HEADER IS OK. GET THE DATA:

try
    data_cel         	= table2cell(sheet_table(2:end,1:4)); % second to last rows are the  data    
    lot_die_data_mat    = cell2matdouble(data_cel);
    
%     for cur_col=1:1:wm_die_cnt
%         cur_wm_name_ndx     = strfind(upper(wm_names_cel{cur_col}),wm_wanted_header);
%         if (~(isempty(cur_wm_name_ndx)))
%             cur_wm_len_value        = str2double(wm_len_cel{cur_col});
%             wafer_map{cur_col}  = cell2matdouble(table2array(sheet_table(3:cur_wm_len_value+2,cur_col))); % 3rd to end is indices.
%         end        
    catch
        error_message = 'ERROR: Invalid Excel file format while  reading data. Fix it and try again';
        return;
end % catch
end % fn wm_get_excel_range
% wafer_map = wafer_map(1:wm_die_cnt);
% wm_names_cel  = wm_names_cel(1:wm_die_cnt);

