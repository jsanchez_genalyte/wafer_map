function [ wm_analysis,wm_analysis_bey,error_message] = perform_wm( ...
    resp_avi_wmot_table,resp_fvi_wmot_table,die_index_avail,die_index_fail,wm_die_ndx,input_params )
%PERFORM_wm performs the requested wm given 2 tables: the available after vi and the failures from vi (visual inwmection)
%   This is the algorithm to handle:
%   replace with neighboors, visual inwmection failures, incomplete wafer maps etc.
% pipeline:  wafer_map (main) -->  wm_part_bin_update --> perform_wm

error_message                   = '';
wm_complete_flag                = false;
wm_analysis                     = struct;
wm_analysis.wm_complete_flag    = false;
wm_analysis.analysis_done       = false;
first_beyond_flag               = false; % assume nothing beyond until something is found beyond.
wm_analysis.wm_use_good_cnt     = 0;
wm_analysis.resp_all_spot_cnt   = 0;
wm_analysis.resp_avi_spot_cnt   = 0;
wm_analysis.resp_fvi_spot_cnt   = 0;
wm_analysis.die_index_wm_ndx_cnt= 0;

wm_analysis.wm_filled_die_cnt   = 0;
wm_analysis.wm_mis_die_cnt      = 0;
wm_analysis.resp_fail_cnt       = 0;
wm_analysis.rpl_from_ok_cnt     = 0;  
wm_analysis.rpl_from_mis_cnt    = 0;

wm_analysis.rpl_from_mis_cnt    = 0;
wm_analysis.rpl_from_mis_cnt    = 0;


first_beyond_index              = 0;
wm_analysis_bey                 = struct; 
wm_analysis_bey.rpl_from_ok_cnt = 0;
wm_analysis_bey.rpl_from_mis_cnt= 0;

if (isempty(wm_die_ndx))
    error_message = 'ERROR: Empty wafer map. Nothing Done';
    return;
end

%good_cnt                = resp_avi_spot_cnt;
resp_avi_spot_cnt        = height(resp_avi_wmot_table);
resp_fvi_spot_cnt    	 = height(resp_fvi_wmot_table);
die_index_wm_ndx_cnt     = size(wm_die_ndx,1);
die_index_wm_ndx_max     = max(wm_die_ndx);               %  && (cur_die_ndx <= die_index_wm_ndx_max)
%die_index_wm_ndx_ori    = wm_die_ndx;

if(resp_avi_spot_cnt ==0)
    error_message = 'ERROR: Work Order Has No die Available to perform wm. Nothing Done';
    return;
end


% Build wm_die_table (some serials will not get set:
serial                   = cell(die_index_wm_ndx_cnt,1); % start with empty serials for all wm
wm_mis_flag              = ones(die_index_wm_ndx_cnt,1); % This is to flag all the wm below and beyond what is on the table (assume missing until found)
rpl_from_flag            = zeros(die_index_wm_ndx_cnt,1);
rpl_with_ndx             = zeros(die_index_wm_ndx_cnt,1);
wm_die_table             = table(serial,wm_die_ndx,wm_mis_flag, rpl_from_flag,rpl_with_ndx);

% extend both tables with flags and die index .. then concatenate them
% FAILURES FROM wmOTTING VISUAL INwmECTION: fvi                             FVI
good_flag               = zeros( resp_fvi_spot_cnt,1);
wm_req_flag             = zeros(resp_fvi_spot_cnt,1);
wm_use_flag             = zeros(resp_fvi_spot_cnt,1);  % used: ie  fullfill in first attemt
wm_mis_flag             = zeros(resp_fvi_spot_cnt,1);  % missed to fullfill in first attempt
die_index               = die_index_fail;
if (isnan(die_index))
    die_index = zeros(0,1);
end
if (isempty(resp_fvi_wmot_table))
    resp_fvi_table = table();
else
    resp_fvi_table          = [table(wm_req_flag) table(wm_use_flag)  table(wm_mis_flag) table(good_flag) table(die_index) resp_fvi_wmot_table(:,1:20) ];
end

% GOOD AFER wmOTTING VISUAL INwmECTION: avi                                  AVI
good_flag               = ones(resp_avi_spot_cnt,1);
wm_req_flag             = zeros(resp_avi_spot_cnt,1);
wm_use_flag             = zeros(resp_avi_spot_cnt,1);
wm_mis_flag             = zeros(resp_avi_spot_cnt,1);
die_index               = die_index_avail;
if (isnan(die_index))
    die_index = zeros(0,1);
end
%die_index_avai_max      = max(die_index_avail);
resp_avi_table          = [table(wm_req_flag) table(wm_use_flag)  table(wm_mis_flag) table(good_flag) table(die_index) resp_avi_wmot_table(:,1:20) ];


resp_all_chip_table     = [ resp_avi_table ; resp_fvi_table];

% create resp_all_chip_table as the concatenation of available and good_flag


% sort the table by die-index
resp_sort_chip_table    = sortrows(resp_all_chip_table,'die_index');

bin_not_from_ndx = resp_sort_chip_table.part_bin ~= input_params.bin_number_from;
if (sum(bin_not_from_ndx))
    % THE COMPLEMENT OF THE BIN_FROM IS NOT EMPTY: Remove them from the table by marking it as no-good
    fail_true_ndx                                     = find(resp_sort_chip_table.good_flag==0); % save it for rpt.
    other_wm_ndx                                      = find(resp_sort_chip_table.part_bin ~= input_params.bin_number_from & resp_sort_chip_table.part_bin ~= num2str(1));
    other_wm_bin                                      = resp_sort_chip_table.part_bin(other_wm_ndx); 
    other_wm_serial                                   = resp_sort_chip_table.serial(other_wm_ndx); 
    resp_sort_chip_table.good_flag(bin_not_from_ndx ) = 0;
else
    fail_true_ndx = nan;
    other_wm_ndx  = nan;
    other_wm_bin  = nan;
    other_wm_serial = nan;
end

% % % % Not a good idea: Try better: mark as failures the ones with not: input_params.bin_number_from
% % % bin_from_ndx = resp_sort_chip_table_all.part_bin == input_params.bin_number_from;
% % % % Build a smaller table with just the records availabe for the wafer map
% % % resp_sort_chip_table    = resp_sort_chip_table_all(bin_from_ndx,:); 


resp_all_chip_cnt       = height(resp_all_chip_table);
rpl_from_flag           = zeros(resp_all_chip_cnt,1);    % replacement: non-zero: FROM == fail+requested
rpl_with_ndx            = zeros(resp_all_chip_cnt,1);    % replacement: non-zero:  WHTH ie the ndx used to replace
wm_avn_flag             = zeros(resp_all_chip_cnt,1);
resp_sort_chip_table    = [resp_sort_chip_table table(rpl_from_flag) table(rpl_with_ndx) table(wm_avn_flag)];


% HAVE SORTED BY DIE_INDEX CHIP TABLE WITH ALL: AVAILABLE AND FAILURE AND WITH 4 flags:
%  wm_req_flag (req by the selected wm)
% ,wm_use_flag (to be filled)
% ,mis_flag    (to be filled)
% ,good_flag   (no failures)


% Do first pass of wafer map: _____________________________________________________________________________ FIRST PASSS _____
% Turn on the ndx with the requested wm;

fit_die_index_wm_ndx = find(wm_die_ndx <= resp_all_chip_cnt);
bey_die_index_wm_ndx = find(wm_die_ndx >  resp_all_chip_cnt);
cros_ndx             = zeros(resp_all_chip_cnt,1);
if (isempty(fit_die_index_wm_ndx))
    % ALL THE REQUESTED DIE FOR THE wafer map are BEYOND THE AVAILABLE CHIP indices:
    % The only way to fix this is (if replacement was requested)
    % 1 - fill as much as possible the request with the available chips.
    % error_message = 'ERROR: jas_tbd: handle logic when all the wm die ndx  are above the available indices';
    wm_die_ndx_last = 0;
    wm_die_table.wm_mis_flag(bey_die_index_wm_ndx) = 1;  % to be solved in pass 2 or 3
    %return;
    %wm_die_table.serial = {}; % (1:wm_die_ndx_last) = resp_all_chip_table.serial(fit_die_index_wm_ndx);
    % do the work with the below indices first:
    if ( length(fit_die_index_wm_ndx) == length(wm_die_ndx))
        % ALL OF THE wm DIE REQUEST ARE WITHIN WHAT IS THERE (avail+fail):
        resp_sort_chip_table.wm_req_flag(wm_die_ndx) = 1;  % flag the ones we need: I.E. All of them
    else
        % SOME OF THE wm DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
        if (isempty(bey_die_index_wm_ndx))
            % ALL THE REQUESTED DIE FOR THE wafer map are BELOW THE AVAILABLE CHIP indices:    NORMAL CASE
            fprintf('\n logic error: we should not get here. Contact Jorge. 858 774 1273\n');
        else
            % SOME OF THE wm DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
            % Mark the beyond only in the wm table
            wm_die_table.wm_mis_flag(bey_die_index_wm_ndx) = 1;  % to be solved in pass 2 or 3
        end
    end
    % HAVE THE wm_req_flag set for the resp_sort_chip_table. Set the cross_ndx
    
    % Build a cross index to locate in the wm any entry in the all_table
    cur_wm_ndx =1;
    for cur_all_ndx =1:1:resp_all_chip_cnt
        if (resp_sort_chip_table.wm_req_flag(cur_all_ndx))
            cros_ndx(cur_all_ndx) =  cur_wm_ndx;
            cur_wm_ndx             = cur_wm_ndx+1;
        end
    end
else
    % SOME OF THE REQUEST INDICES ARE BELOW THE END OF THE TABLE:
    % update serial for the wm_die_table
    wm_die_ndx_last =length(fit_die_index_wm_ndx);
    wm_die_table.serial(1:wm_die_ndx_last) = resp_all_chip_table.serial(wm_die_ndx(fit_die_index_wm_ndx));  % jas_fix_2018_04_10 vs:    resp_all_chip_table.serial(fit_die_index_wm_ndx);
    
    % do the work with the below indices first:
    if ( length(fit_die_index_wm_ndx) == length(wm_die_ndx))
        % ALL OF THE wm DIE REQUEST ARE WITHIN WHAT IS THERE (avail+fail):
        resp_sort_chip_table.wm_req_flag(wm_die_ndx) = 1;  % flag the ones we need: I.E. All of them
    else
        % SOME OF THE wm DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
        % jas_tbd: Maybe the next line is not needed because it is not used jas_tbd_verify
        resp_sort_chip_table.wm_req_flag(wm_die_ndx(fit_die_index_wm_ndx)) = 1;  % flag subset: some of the ones we need.
        if (isempty(bey_die_index_wm_ndx))
            % ALL THE REQUESTED DIE FOR THE wafer map are BELOW THE AVAILABLE CHIP indices:    NORMAL CASE
            fprintf('\n logic error: we should not get here. Contact Jorge. 858 774 1273\n');
        else
            % SOME OF THE wm DIE REQUEST ARE ABOVE WHAT IS AVAILABLE: (avail+fail) Mark as request only what can be done directly
            % Mark the beyond only in the wm table
            wm_die_table.wm_mis_flag(bey_die_index_wm_ndx) = 1;  % to be solved in pass 2 or 3
        end
    end
    % HAVE THE wm_req_flag set for the resp_sort_chip_table. Set the cross_ndx
    
    % Build a cross index to locate in the wm any entry in the all_table
    cur_wm_ndx =1;
    for cur_all_ndx =1:1:resp_all_chip_cnt
        if (resp_sort_chip_table.wm_req_flag(cur_all_ndx))
            cros_ndx(cur_all_ndx) =  cur_wm_ndx;
            cur_wm_ndx             = cur_wm_ndx+1;
        end
    end
    
end % non empty fit die index

% FIRST PASS: COMPLETE. Do second pass ...
% Do second pass of wafer map: _____________________________________________________________________________ SECOND PASSS _____

if ((resp_avi_spot_cnt >= die_index_wm_ndx_cnt) && (resp_all_chip_cnt >= die_index_wm_ndx_max))
    % wm HAS NOT TOO MANY DAY AND REQUESTED INDICES ARE NOT TOO LARGE:
    % we may be able to do a complete wm (144/204) deppending on failures 
    % Check if within the requested there are failures: wm_use_flag == requested and good good_flag
    req_and_good_ndx                                    = resp_sort_chip_table.wm_req_flag(wm_die_ndx) & resp_sort_chip_table.good_flag(wm_die_ndx);
    resp_sort_chip_table.wm_use_flag(wm_die_ndx(req_and_good_ndx))  = 1;
    resp_sort_chip_table.wm_mis_flag(wm_die_ndx(req_and_good_ndx))  = 0;
    wm_die_table.wm_mis_flag(req_and_good_ndx)                      = 0; %jas_fix_rev_107  2018_03_28
    
    use_pass_one_cnt                                    = sum(resp_sort_chip_table.wm_use_flag);
    if (use_pass_one_cnt == die_index_wm_ndx_cnt)
        % GOT IT ALL USE IS GOOD AND WE COVERED THE WHOLE wm: No miss and no need for replacements.
        wm_complete_flag                = true;
        wm_analysis.analysis_done       = true;
    else
        % DID NOT GET ALL WHAT IT WAS REQUESTED: SOME of the req IS not GOOD AND WE did not COVER THE WHOLE wm
        if (~(input_params.replace_chip_if_miss_flag ) )
            % USER DOES NOT WANT TO REPLACE MISSING
            % Determine which ones require replacement with neighboors:
            wm_complete_flag            = false;
            wm_analysis.analysis_done   = true;
        else
            % USER REQUESTED TO REPLACE MISSING: requested and not used
            resp_sort_chip_table.wm_mis_flag(wm_die_ndx)        = resp_sort_chip_table.wm_req_flag(wm_die_ndx) & (~(resp_sort_chip_table.wm_use_flag(wm_die_ndx)));
            wm_mis_cnt                                          = sum(resp_sort_chip_table.wm_mis_flag);
            resp_sort_chip_table.rpl_from_flag                  = resp_sort_chip_table.wm_mis_flag; %  start the replace from: with all the missing. to being with
            % Determine available neighboors: not used yet but good (all of these must be among the not requested: othewhise they would had been used)
            resp_sort_chip_table.wm_avn_flag                    = (~(resp_sort_chip_table.wm_use_flag)) & (resp_sort_chip_table.good_flag);
            wm_avn_cnt                                          = sum(resp_sort_chip_table.wm_avn_flag);
            if (wm_avn_cnt < wm_mis_cnt)
                % DID NOT GET IT: Not enough neighboors: Use them all: wm_avn_cnt     AVN == Available Neighbor.
                wm_complete_flag                                                    = false;
                resp_sort_chip_table.wm_use_flag( resp_sort_chip_table.wm_avn_flag) = 1; % does this stat. modifies only the ones?
                resp_sort_chip_table.wm_mis_flag(resp_sort_chip_table.wm_avn_flag)  = 0;
                wm_die_table.wm_mis_flag(cros_ndx(resp_sort_chip_table.wm_avn_flag))= 0;
                %only true replacements: resp_sort_chip_table.rpl_with_ndx(resp_sort_chip_table.wm_mis_flag) ~=0  fill in the "with"  with the available neighboors.
            else
                % GOT IT: ENOUGH neighboors: Use a subset. only wm_mis_cnt of them and the closest ones.
                wm_complete_flag        = true;
                %ava_size               = size(ava_ndx,1);
                %ava_back_flag          = zeros(ava_size,1);
                for cur_mis_ndx =1:1:wm_mis_cnt
                    % find nearest neighboor for this missing among the available ones.
                    % As you find them mark the wm_use_flag to 1 for the found nearerst neightboor.
                    [ resp_sort_chip_table,wm_die_table,found_repl_flag ] = find_closest_nei( resp_sort_chip_table,wm_die_table,cros_ndx );
                    if (~(found_repl_flag))
                        break;
                    end
                end % for each missing  tag: .wm_avn_flag     ava_ndx(ndx_1)
                % AT THIS POINT EACH MISSING SHOULD HAD BEEN REPLACED WITH THE NEAREST NEIGHBOOR: Evaluate
                if ( (sum(resp_sort_chip_table.wm_mis_flag) == 0) && (sum(resp_sort_chip_table.wm_use_flag) == die_index_wm_ndx_cnt))
                    %
                    fprintf('Succsess finding neightboors. wm WAS FULLY SATISFIED. See: wm_use_flag for used die.');
                end
            end % wm_avn_cnt > wm_mis_cnt (using only some of the neigh)
        end % USER req replace if missing.
    end % DID NOT GET IT all of the requested so some is not GOOD AND WE did not COVER THE WHOLE wm
else  % 48 /207
    % WE CAN **not** DO A COMPLETE wm. 3 possible cases but we do not care to know why. Just fill in as much as we can:
    if ( (~(input_params.wm_ok_do_partially_flag)) && (~(input_params.replace_chip_if_miss_flag)))
        % BAD SHAPE AND TOO RESTRICTIVE: can not do anyting to fix problem
        wm_complete_flag        = false;
    end
    if ( input_params.wm_ok_do_partially_flag)  
        % DOING_PARTIAL REQUEST AND MAY OR MAT NOT BE OK TO REPLACE IF MISSING
        %   Possible causes to prevent doing a full wm:
        %   1 - cnt     of avail is less than cnt     of wm: if doing_partial request:  trim wm to cnt_of avail
        %   2 - max ndx of avail is less than max ndx of wm: if doing_partial request:  trim wm to max_die_ndx <= max ndx of avail
        %   3 - 1 and 2: trim wm to the smallest of 1 and 2.
        % FIRST PASS INCOMPLETE:                                             ***************************    first pass incomplete **********************
        for cur_die_ndx = 1:1: die_index_wm_ndx_cnt
            % PROCESS EACH REQUEST ONE AT A TIME: first pass
            
            cur_die_req_ndx = wm_die_ndx(cur_die_ndx);   
            % check if good:
            if (cur_die_req_ndx <= resp_all_chip_cnt)
                if  (resp_sort_chip_table.good_flag(cur_die_req_ndx))
                    % WITHIN BOUNDS AND GOOD: Use it
                    resp_sort_chip_table.wm_use_flag(cur_die_req_ndx)       = 1;
                    resp_sort_chip_table.wm_mis_flag(cur_die_req_ndx)       = 0;
                    wm_die_table.wm_mis_flag(cros_ndx(cur_die_req_ndx))     = 0;
                    % no need for replace_from and with because it is available: there is no replacement here.
                else
                    % FAIL: Mark it as missing and skip it for the first pass:
                    resp_sort_chip_table.wm_mis_flag(cur_die_req_ndx)       = 1;
                    % mark it as needed to be replaced from: (without a replace with)
                    if (input_params.replace_chip_if_miss_flag)
                        % OK TO DO REPLACEMENT FOR MISSING: 
                        resp_sort_chip_table.rpl_from_flag(cur_die_req_ndx)     = 1;
                        wm_die_table.wm_mis_flag(cros_ndx(cur_die_req_ndx))     = 1;
                        wm_die_table.rpl_from_flag(cros_ndx(cur_die_req_ndx))   = 1;
                    end
                end
            else
                % JAS_HERE HANDLE REQUESTS BEYOND ... tues_eve
                % build list of die for the third pass: pass beyond what is on the  table
                % find the position in the wm for this index;
                % cur_wm_ndx = find(wm_die_table.wm_die_ndx ==cur_die_req_ndx,1);

                if (~(first_beyond_flag))
                    first_beyond_flag = true;
                    first_beyond_index = cur_die_req_ndx;
                end
                wm_die_table.wm_mis_flag(cur_die_ndx) = 1;
                if (input_params.replace_chip_if_miss_flag)
                    % OK TO DO REPLACEMENT FOR MISSING: check for the last available from the all table
                    resp_sort_chip_table.wm_avn_flag = (~(resp_sort_chip_table.wm_use_flag)) & (resp_sort_chip_table.good_flag);
                    ava_ndx                          = find(resp_sort_chip_table.wm_avn_flag == 1); %  = (~(resp_sort_chip_table.wm_use_flag)) & (resp_sort_chip_table.good_flag);
                    if (~(isempty(ava_ndx)))
                        % HAVE AVAILABLE: Take the last one (because we are beyond.
                        ava_ndx = ava_ndx(end);
                        wm_die_table.rpl_from_flag(cur_die_ndx)                 = 1;
                        wm_die_table.rpl_with_ndx(cur_die_ndx)                  = ava_ndx;
                        wm_die_table.wm_mis_flag(cur_die_ndx)                   = 0;           % as replacement was found: No failure anymore
                        resp_sort_chip_table.wm_mis_flag(ava_ndx)               = 0;           % as replacement was found: No failure anymore
                        resp_sort_chip_table.wm_use_flag(ava_ndx)               = 1;           % mark it as used, so it does no keep using the same one
                        % get the serial of the from
                        wm_die_table.serial(cur_die_ndx)                        = resp_sort_chip_table.serial(ava_ndx);
                    else
                        break;  % nothing found to work with... we must be done.
                    end
                end
             end
        end % for each die requested in the wm.
        
        if (first_beyond_flag)
            fprintf('\n Found wafer map (wm) indices: \t\t\t  From  %-4d To %-4d are BEYOND the Available WO largest Index: %-4d\n'...
                ,first_beyond_index, wm_die_ndx(die_index_wm_ndx_cnt),   resp_all_chip_cnt);
        end
        
        % AT THIS POINT WE HAVE: What is used and what is missed due to failures or beyond: First pass.
        % DO A SECOND PASS: Fill the wholes as much as you can.
        % && (cur_die_ndx <= die_index_wm_ndx_max)
        %fprintf('done with first pass\n');
        for cur_die_ndx = 1:1: die_index_wm_ndx_cnt
            % PROCESS EACH REQUEST ONE AT A TIME: second pass  ***************************    second pass incomplete **********************
            
            if ((~(wm_die_table.wm_mis_flag(cur_die_ndx))) || (~(input_params.replace_chip_if_miss_flag)  )  )
                % THIS DIE IS NOT MISSING.  OR ( IT IS MISSING BUT IS NOT OK TO DO REPLACEMENTS: skip
                continue; % This one is not missing (set at first pass) go for the next one.
            end
            cur_die_req_ndx = wm_die_ndx(cur_die_ndx);
            if ( (cur_die_req_ndx > resp_all_chip_cnt ) || ( cur_die_ndx > resp_all_chip_cnt ))
                % REACHED END OF TABLE: wm is asking for more thant what is there.
                % check if there is ANYTHING NOT USED YET:
                avai_ndx = find(resp_sort_chip_table.good_flag ==1 & resp_sort_chip_table.wm_use_flag ==0);
                if (isempty(avai_ndx))
                    % COULD NOT FIND ANYTHING TO FULLFILL THIS MISSING: We are done: Get out.
                    break;
                end
                % FOUND EMPTY SLOT: replacement pass BEYOND all table: use wm_die_table to record it:
                % THE FROM IS BEYOND !!!! the end of the table. only the wm_use_flag is within.
                avai_ndx                                       = avai_ndx(1);
                wm_die_table.wm_mis_flag(cur_die_ndx)          = 0;            % it is not missing anymore
                resp_sort_chip_table.wm_use_flag(avai_ndx)     = 1;            % it is used now.
                wm_die_table.rpl_from_flag(cur_die_ndx)        = 1;            % it is replaced now
                wm_die_table.c(cur_die_ndx)         = avai_ndx;     % it is the replaced with.
                continue; % this one is done
            end
            % THIS ONE IS MISSING: IT NEEDS TO BE REPLACED IF IT CAN BE DONE:  Find the closest neigh good that is not in use:
            [ resp_sort_chip_table,wm_die_table,found_repl_flag ] = find_closest_nei( resp_sort_chip_table,wm_die_table,cros_ndx );
            if (~(found_repl_flag))
                break;
            end
        end % for each die from wm: second pass.
        % for all cases: Use every good die. period.  no need for more logic
        % jas_here: do not use indiscriminately:  resp_sort_chip_table.wm_use_flag   = resp_sort_chip_table.good_flag; %best delivery effort.
    end % input_params.wm_ok_do_partially_flag && ok to do replacement
end   %  WE can NOT  DO A COMPLETE wm

% BEYOND LAST PASS:       _______________________________________________________________________________________________________  BEYOND

% if (~(isempty(bey_die_index_wm_ndx)))
%     % SOME wm DIE ARE BEYOND THE ALL TABLE: 
%     % pull from the end: ie the last available 
%     bey_die_index_wm_ndx = bey_die_index_wm_ndx(end:-1:1);
%     for cur_wm_die = 1:1:length(bey_die_index_wm_ndx)
%         
%         
%     end % for each request beyond all
% end

% save cnts for reporting _______________________________________________________________________________________________________

wm_analysis.resp_avi_spot_cnt   = resp_avi_spot_cnt;                    % Available     chip count  (good after visual inwmection)
wm_analysis.resp_fvi_spot_cnt   = resp_fvi_spot_cnt;                    % Failures      chip count  (fail after visual inwmection)
wm_analysis.resp_all_spot_cnt   = resp_avi_spot_cnt +resp_fvi_spot_cnt; % Total cnt: good + fail
wm_analysis.die_index_wm_ndx_cnt= die_index_wm_ndx_cnt;                 % wm Requested  chip count
wm_analysis.wm_die_ndx          = wm_die_ndx;

% rpt failures: ndx and serial
fail_ndx                        = find(resp_sort_chip_table.good_flag==0);

if (isnan(fail_true_ndx))
    wm_analysis.resp_fail_cnt   = 0;
else
    wm_analysis.resp_fail_cnt   = length(fail_true_ndx);
end
wm_analysis.resp_fail_ndx       = fail_true_ndx;
if (isnan(fail_true_ndx))
    wm_analysis.other_wm_cnt    = 0;
else
    wm_analysis.other_wm_cnt    = length(other_wm_ndx);
end
wm_analysis.other_wm_ndx        = other_wm_ndx; 
wm_analysis.other_wm_bin        = other_wm_bin;
wm_analysis.other_wm_serial     = other_wm_serial;
if (isempty(resp_fvi_wmot_table))
    wm_analysis.resp_fail_serial= {};    
else
    wm_analysis.resp_fail_serial= resp_fvi_wmot_table.serial;
end
% wm_analysis.resp_fail_ndx(1), char(wm_analysis.resp_fail_serial(1)));

% evaluate results:
wm_filled_die_cnt               = sum(resp_sort_chip_table.wm_use_flag);  % wm filled  chip count
wm_mis_die_cnt                  = sum(resp_sort_chip_table.wm_mis_flag);  % wm miss  chip count
wm_mis_die_ndx                  = find(resp_sort_chip_table.wm_mis_flag == 1);  % wm miss  chip indices
wm_filled_die_index             = resp_sort_chip_table.die_index(logical(resp_sort_chip_table.wm_use_flag));
wm_use_all_ndx                  = find(resp_sort_chip_table.wm_use_flag ==1);
% determine wm_use_good_ndx
% get the good ones first

wm_use_good_ndx_ndx             = (resp_sort_chip_table.wm_use_flag ==1) & (resp_sort_chip_table.good_flag ==1) ;
wm_use_good_ndx                 = find(wm_use_good_ndx_ndx == 1);
wm_analysis.wm_complete_flag    = wm_complete_flag;    % partial wm or camplete
wm_analysis.wm_filled_die_cnt   = wm_filled_die_cnt;   % filled
wm_analysis.wm_mis_die_cnt      = wm_mis_die_cnt;      % missing to be filled
wm_analysis.wm_mis_die_ndx      = wm_mis_die_ndx;      % missing to be filled
wm_analysis.wm_filled_die_index = wm_filled_die_index; % selected die for the wm.
wm_analysis.wm_use_all_ndx      = wm_use_all_ndx;      % ndx of used for wm from all (before wmotting)

if (~(isempty(wm_use_good_ndx)))
    wm_analysis.wm_use_good_cnt     = length(wm_use_good_ndx);
    wm_analysis.wm_use_good_ndx     = wm_use_good_ndx;     % ndx of used for wm from the good ones
    wm_analysis.wm_use_good_serial  = resp_sort_chip_table.serial(wm_use_good_ndx);  % serials used for wm from the good ones
else
    % FOUND NOTHING TO MOVE:
    wm_analysis.wm_use_good_cnt     = 0;
    wm_analysis.wm_use_good_ndx     = nan;     % ndx of used for wm from the good ones
    wm_analysis.wm_use_good_serial  = {};  % serials used for wm from the good ones
    
end

%resp_sort_chip_table.rpl_from_ok_ndx:      REPACEMENT: SUCCESS WITHIN
wm_analysis.rpl_from_ok_ndx     = find(resp_sort_chip_table.rpl_from_flag ==1 & resp_sort_chip_table.rpl_with_ndx ~=0 );
wm_analysis.rpl_from_ok_serial  = resp_sort_chip_table.serial(      wm_analysis.rpl_from_ok_ndx);
wm_analysis.rpl_with_ok_ndx     = resp_sort_chip_table.rpl_with_ndx(wm_analysis.rpl_from_ok_ndx);
wm_analysis.rpl_with_ok_serial  = resp_sort_chip_table.serial(resp_sort_chip_table.rpl_with_ndx(wm_analysis.rpl_from_ok_ndx));
wm_analysis.rpl_from_ok_cnt     = length(wm_analysis.rpl_from_ok_ndx);

%resp_sort_chip_table.rpl_from_miss_ndx:    REPACEMENT: MISS. WITHIN
wm_analysis.rpl_from_mis_ndx  	= find(resp_sort_chip_table.rpl_from_flag ==1 & resp_sort_chip_table.rpl_with_ndx ==0 );
wm_analysis.rpl_from_mis_cnt   	= length(wm_analysis.rpl_from_mis_ndx);
wm_analysis.rpl_from_mis_serial	= resp_sort_chip_table.serial(wm_analysis.rpl_from_mis_ndx);

% BEYOND: wm_analysis_bey:
% fix: 2018_04_10: Alex wants the die to come in original order: The beyond come backwards: 
%                  dis is due that for the first beyond we look for the last available, the second beyond gets the previous to last availabe and so on...
% i.e.
%   REPLACEMENT DIE OK:           AVAILABLE
%         index_from      index_with      serial_from             serial_replace      BEYOND
%         245                     242                     P153060.07.1096         P153060.07.1096
%         247                     241                     P153060.07.1095         P153060.07.1095
%         249                     240                     P153060.07.1094         P153060.07.1094
% but Alex wants: (maintain order)
%    REPLACEMENT DIE OK:           AVAILABLE
%         index_from      index_with      serial_from             serial_replace      BEYOND
%         245                     240                     P153060.07.1094         P153060.07.1094
%         247                     241                     P153060.07.1095         P153060.07.1095
%         249                     242                     P153060.07.1096         P153060.07.1096
% Solution: sort the rpl_with_ndx for the  beyond part of the wm_die_table in ascending order. 
% wm_die_table = 
%          serial          wm_die_ndx    wm_mis_flag    rpl_from_flag    rpl_with_ndx
%     _________________    __________    ___________    _____________    ____________
%     'P153060.07.0865'     11           0              1                  0         
%     'P153060.07.0866'     49           0              0                  0         
%     'P153060.07.0867'     62           0              0                  0         
%     'P153060.07.0868'    111           0              0                  0         
%     'P153060.07.0869'    132           0              0                  0         
%     'P153060.07.1096'    245           0              1                242    becomes: 240     
%     'P153060.07.1095'    247           0              1                241         
%     'P153060.07.1094'    249           0              1                240    becomes: 242

if (~(isempty(bey_die_index_wm_ndx)))
    wm_die_table_bey       = wm_die_table(bey_die_index_wm_ndx,:);
    rpl_with_ndx_descend   = wm_die_table_bey.rpl_with_ndx;  
    rpl_with_ndx_ascend    = sort(rpl_with_ndx_descend);
    wm_die_table.rpl_with_ndx(bey_die_index_wm_ndx)  = rpl_with_ndx_ascend;
end

if (~(isempty(bey_die_index_wm_ndx)))
    % SOMETHING TO RPT: there are wm die beyond ALL table
    
    %wm_die_table_bey = wm_die_table(1:length(fit_die_index_wm_ndx);
    % REPLACEMENT SUCCESS: BEYOND TABLE LIMITS:
    wm_analysis_bey.rpl_from_ok_ndx         = find(wm_die_table.rpl_from_flag ==1 & wm_die_table.rpl_with_ndx ~=0 );
    if (~(isempty(wm_analysis_bey.rpl_from_ok_ndx )))
        keep_ndx = wm_analysis_bey.rpl_from_ok_ndx > wm_die_ndx_last;
        wm_analysis_bey.rpl_from_ok_ndx         = wm_analysis_bey.rpl_from_ok_ndx(keep_ndx); % keep only the true beyond
        if (~(isempty(wm_analysis_bey.rpl_from_ok_ndx)))
            wm_analysis_bey.rpl_from_ok_serial  = wm_die_table.serial(      wm_analysis_bey.rpl_from_ok_ndx);
            wm_analysis_bey.rpl_with_ok_ndx     = wm_die_table.rpl_with_ndx(wm_analysis_bey.rpl_from_ok_ndx);
            wm_analysis_bey.rpl_with_ok_serial  = wm_die_table.serial(      wm_analysis_bey.rpl_from_ok_ndx);
            wm_analysis_bey.rpl_from_ok_cnt     = length(                   wm_analysis_bey.rpl_from_ok_ndx);
        end
    end
    
    % REPLACEMENT MISS: BEYOND TABLE LIMITS:
    wm_analysis_bey.rpl_from_mis_ndx        = find(wm_die_table.rpl_from_flag ==1 & wm_die_table.rpl_with_ndx ==0 );
    if (~(isempty(wm_analysis_bey.rpl_from_mis_ndx )))
        keep_ndx = wm_analysis_bey.rpl_from_mis_ndx > wm_die_ndx_last;
        wm_analysis_bey.rpl_from_mis_ndx         = wm_analysis_bey.rpl_from_mis_ndx(keep_ndx); % keep only the true beyond
        if (~(isempty(wm_analysis_bey.rpl_from_mis_ndx )))
            wm_analysis_bey.rpl_from_mis_ndx    = wm_analysis_bey.rpl_from_mis_ndx(wm_die_ndx_last+1:end);
            wm_analysis_bey.rpl_from_mis_cnt    = length(wm_analysis_bey.rpl_from_mis_ndx);
            wm_analysis_bey.rpl_from_mis_serial = wm_die_table.serial(wm_analysis_bey.rpl_from_mis_ndx);
        end
    end
end

% finall summary:
if (wm_analysis.wm_filled_die_cnt  == die_index_wm_ndx_cnt) 
    % wm_filled_die_cnt is total: from before and beyond!!! 
    wm_analysis.wm_complete_flag = true;
else
    wm_analysis.wm_complete_flag = false;
end

end % fn perform wm _____________________________________________________________________________________________________________________





