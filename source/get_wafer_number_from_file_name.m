function [ wafer_number,wafer_lot_number_str,error_message ] = get_wafer_number_from_file_name( a_file_name )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

wafer_number        = 0;
wafer_lot_number_str= '';
error_message       = '';
wafer_num_end_ndx   = strfind(a_file_name,'.xlsx');
if ((~(wafer_num_end_ndx)) || (wafer_num_end_ndx < 8))
    error_message   = sprintf('Invalid file name: %-s \n could not get wafer number',a_file_name);
    return;
end

temp_str            = a_file_name(wafer_num_end_ndx-8:wafer_num_end_ndx-1);
temp_str            = strrep(temp_str,'Wafer','');
wafer_number        = str2double(temp_str);
%wafer_number_str    = num2str(wafer_number);

lot_num_start_ndx   = strfind(a_file_name,' Lot ');
wafer_num_start_ndx = strfind(a_file_name,' Wafer');
wafer_lot_number_str= a_file_name(lot_num_start_ndx+5:wafer_num_start_ndx-1);

if (isnan(wafer_number) || (wafer_number == 0))
    error_message   = sprintf('Invalid file name: %-s \n could not get wafer number',a_file_name);
    return;
end



end % fn get_wafer_number_from_file_name
