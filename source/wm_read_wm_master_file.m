function  [ wafer_map,wm_names_cel,  error_message ]  = wm_read_wm_master_file(lot_die_directory_name)
%wm_READ_wm_MASTER_FILE   reads the wafer_map master file and returns a list with the columns.
% Each column represents a wafer_map. 
% The lot_die_directory_name contains the full path to the file.
% Nick is the owner of the file, and He will periodically updates the file as needed. 

% JAS_Temporal: Rather than reading the file, right now I am hardcoding the first 4 wafer_maps. jas_tbd.
% Sample call: 
% [ wafer_map,  error_message ]  = wm_read_wm_master_file('C:\Users\jsanchez\Documents\smpling_plan\configure\wafer_map_master.xlsm');
% pipeline:    wafer_map  --> wm_part_bin_update  -->  wm_read_wm_master_file  -->  wm_get_excel_range
fprintf('\n Master wafer_map File ');
%fprintf('\n                             = %-s\n',lot_die_directory_name);

% call script to collect and copy the 23 wafer files form the network drive to the local dir.


% Get all the file names from the directory:


[ wafer_map, wm_names_cel, error_message ] = wm_get_excel_range( lot_die_directory_name,1);
if ((isempty(error_message)) || (strcmp(error_message(1:7),'WARNING')) )
    %fprintf('\n DONE  ... reading wafer_maps: \t   %-3d wafer_maps\n',size(wafer_map,2));
    fprintf('\n wafer_maps read count   = %-3d ',size(wafer_map,2));      
    fprintf('\n Reading wafer_maps    ... DONE           ');        
    return;
end
end % fn wm_read_wm_master_file 
 
% % 
% % 
% % wafer_map   = {};
% % error_message   = '';
% % % TBD:
% % % 1 - Open File
% % % 2 - Read tab 1
% % % 3 - Close File.
% % % 4 - packl contents into a list. 
% % 
% % wafer_map{1}= [...
% %     11
% %     49
% %     62
% %     111
% %     132
% %     181
% %     194
% %     232
% %     ];
% % 
% % wafer_map{2}= [ ...
% %     12
% %     50
% %     63
% %     112
% %     133
% %     182
% %     195
% %     233
% %     ];
% % 
% % wafer_map{3}= [ ...
% %     1
% %     3
% %     5
% %     7
% %     9
% %     13
% %     15
% %     17
% %     19
% %     21
% %     23
% %     25
% %     27
% %     29
% %     31
% %     33
% %     35
% %     37
% %     39
% %     41
% %     43
% %     45
% %     47
% %     51
% %     53
% %     55
% %     57
% %     59
% %     61
% %     65
% %     67
% %     69
% %     71
% %     73
% %     75
% %     77
% %     79
% %     81
% %     83
% %     85
% %     87
% %     89
% %     91
% %     93
% %     95
% %     97
% %     99
% %     101
% %     103
% %     105
% %     107
% %     109
% %     113
% %     115
% %     117
% %     119
% %     121
% %     123
% %     125
% %     127
% %     129
% %     131
% %     135
% %     137
% %     139
% %     141
% %     143
% %     145
% %     147
% %     149
% %     151
% %     153
% %     155
% %     157
% %     159
% %     161
% %     163
% %     165
% %     167
% %     169
% %     171
% %     173
% %     175
% %     177
% %     179
% %     183
% %     185
% %     187
% %     189
% %     191
% %     193
% %     197
% %     199
% %     201
% %     203
% %     205
% %     207
% %     209
% %     211
% %     213
% %     215
% %     217
% %     219
% %     221
% %     223
% %     225
% %     227
% %     229
% %     231
% %     235
% %     237
% %     239
% %     241
% %     ];
% % 
% % wafer_map{4}= [ ...
% %     2
% %     4
% %     6
% %     8
% %     10
% %     14
% %     16
% %     18
% %     20
% %     22
% %     24
% %     26
% %     28
% %     30
% %     32
% %     34
% %     36
% %     38
% %     40
% %     42
% %     44
% %     46
% %     48
% %     52
% %     54
% %     56
% %     58
% %     60
% %     64
% %     66
% %     68
% %     70
% %     72
% %     74
% %     76
% %     78
% %     80
% %     82
% %     84
% %     86
% %     88
% %     90
% %     92
% %     94
% %     96
% %     98
% %     100
% %     102
% %     104
% %     106
% %     108
% %     110
% %     114
% %     116
% %     118
% %     120
% %     122
% %     124
% %     126
% %     128
% %     130
% %     134
% %     136
% %     138
% %     140
% %     142
% %     144
% %     146
% %     148
% %     150
% %     152
% %     154
% %     156
% %     158
% %     160
% %     162
% %     164
% %     166
% %     168
% %     170
% %     172
% %     174
% %     176
% %     178
% %     180
% %     184
% %     186
% %     188
% %     190
% %     192
% %     196
% %     198
% %     200
% %     202
% %     204
% %     206
% %     208
% %     210
% %     212
% %     214
% %     216
% %     218
% %     220
% %     222
% %     224
% %     226
% %     228
% %     230
% %     234
% %     236
% %     238
% %     240
% %     242
% %     ];
% % 
% % wafer_map{11}= [ ...
% % 1
% % 3
% % 5
% % 7
% % 9
% % 12
% % 14
% % 16
% % 18
% % 20
% % 23
% % 25
% % 27
% % 29
% % 32
% % 34
% % 36
% % 38
% % 40
% % 43
% % 45
% % 47
% % 49
% % 52
% % 54
% % 56
% % 58
% % 60
% % 63
% % 65
% % 67
% % 69
% % 72
% % 74
% % 76
% % 78
% % 80
% % 83
% % 85
% % 87
% % 89
% % 92
% % 94
% % 96
% % 98
% % 100
% % 103
% % 105
% % 107
% % 109
% % 112
% % 114
% % 116
% % 118
% % 120
% % 123
% % 125
% % 127
% % 129
% % 131
% % 134
% % 136
% % 138
% % 140
% % 143
% % 145
% % 147
% % 149
% % 151
% % 154
% % 156
% % 158
% % 160
% % 163
% % 165
% % 167
% % 169
% % 171
% % 174
% % 176
% % 178
% % 180
% % 183
% % 185
% % 187
% % 189
% % 191
% % 194
% % 196
% % 198
% % 200
% % 203
% % 205
% % 207
% % 209
% % 211
% % 214
% % 216
% % 218
% % 220
% % 223
% % 225
% % 227
% % 229
% % 231
% % 234
% % 236
% % 238
% % 240
% % ];
% % 
% % wafer_map{12}= [ ...
% % 2
% % 15
% % 26
% % 39
% % 50
% % 62
% % 75
% % 86
% % 99
% % 110
% % 124
% % 135
% % 148
% % 159
% % 172
% % 184
% % 195
% % 208
% % 219
% % 232
% %    ]; 



