function [  ] = wm_plot_contour( plot_data_info_struct,wafer_map_mat,input_params,close_all_true_fig_flag )
%WM_PLOT_contour produces 3D plot: Z is FSR X,Y are wafer coordinates
%   The input struct must have the requiered fields.
% sample call:   [  ] = wm_plot_contour( plot_data_info_struct );
col_ndx_row                     = 2;    % excel deppendency: order of cols in excel file
col_ndx_col                     = 3;
col_ndx_fsr                     = 4;
figure_title                    = plot_data_info_struct.figure_title;
plot_title                      = plot_data_info_struct.plot_title;
xlabel_plot                     = plot_data_info_struct.xlabel_plot;
ylabel_plot                     = plot_data_info_struct.ylabel_plot;
zlabel_plot                     = plot_data_info_struct.zlabel_plot;
fsr_heat_min                    = input_params.fsr_heat_min;                    % = 5.3;  
fsr_heat_max                    = input_params.fsr_heat_max;                    % = 5.8; 
wanted_contour_values_cnt_fsr  	= input_params.wanted_contour_values_cnt_fsr;   % = 0;  
wanted_contour_delta_fsr        = input_params.wanted_contour_delta_fsr;        % = .05;
% S4.Fill contour map
%min_wafer                      = min(wafer_map_mat);
max_wafer                       = max(wafer_map_mat);
die_all_cnt                     = size(wafer_map_mat,1);

contour_data    = nan(max_wafer(col_ndx_row),max_wafer(col_ndx_col));
for cur_die_ndx = 1:1:die_all_cnt
    cur_row     = wafer_map_mat(cur_die_ndx,col_ndx_row);
    cur_col     = wafer_map_mat(cur_die_ndx,col_ndx_col);
    cur_fsr     = wafer_map_mat(cur_die_ndx,col_ndx_fsr);
    if (isfinite(cur_row) && isfinite(cur_col) && isfinite(cur_fsr) )
        contour_data(cur_row  ,cur_col )=  cur_fsr;
    end
end
% clean figure: _____________________________________________________________
if close_all_true_fig_flag
    close_all_true_figures;
end
fig_btm                = 10;
fig_left               = 50;
fig_height             = 800; %  1100: works fine in my screen.
fig_width              = round(fig_height * (60/50));

hFig                   = figure('Name',figure_title);
set(hFig, 'Position', [fig_left fig_btm  fig_width fig_height]);

%contour_axes                  = gca;
% scatter3(contour_axes,wafer_map_mat(:,2),wafer_map_mat(:,3),wafer_map_mat(:,4)...
%     ,'LineWidth'        ,0.2...
%     ,'MarkerFaceColor'  ,'Red'...
%     ,'MarkerEdgeColor'  ,'Red'...
%     ,'Marker'           ,'s'...
%     ,'SizeData',9); % ,'MarkerFaceColor',dark_green_color);

max_fsr               = nanmax(wafer_map_mat(:,4));
min_fsr               = nanmin(wafer_map_mat(:,4));

ploted_contour_values_cnt       = 10;     % default is 10 contour lines.
if (wanted_contour_values_cnt_fsr)
    % USER SPECIFIED COUNT OF FSR CONTOUR LINES
    ploted_contour_values_cnt   = wanted_contour_values_cnt_fsr;
end

% CHECK FOR VALID FSR DATA:
if (min_fsr < fsr_heat_min)
    % FOUND VALUES OUTSIDE THE SPECIFIED MIN FSR: Report them
    fprintf('\nWARNING: Found FSR values Outside the SPECIFIED  MIN_FSR value: %-7.3f',fsr_heat_min);
    fprintf('\nDIE_INDEX \t\tDIE_ROW \t\tDIE_COL \t\tFSR');
    min_outside_ndx = wafer_map_mat(:,4) < fsr_heat_min;
    cnt_min         = sum(min_outside_ndx);
    mat_min         = wafer_map_mat(min_outside_ndx,:);
    for cur_min_ndx = 1:1:cnt_min
        fprintf('\n%-4d \t\t\t %-4d \t\t\t %-4d  \t\t\t %-7.4f',mat_min(cur_min_ndx,1),mat_min(cur_min_ndx,2),mat_min(cur_min_ndx,3),mat_min(cur_min_ndx,4));
    end
    fprintf('\n');
end

if (max_fsr > fsr_heat_max)
    % FOUND VALUES OUTSIDE THE SPECIFIED MIN FSR: Report them
    fprintf('\nWARNING: Found FSR values Outside the SPECIFIED  MIN_FSR value: %-7.3f',fsr_heat_max);
    fprintf('\nDIE_INDEX \t\tDIE_ROW \t\tDIE_COL \t\tFSR');
    max_outside_ndx = wafer_map_mat(:,4) < fsr_heat_max;
    cnt_max         = sum(max_outside_ndx);
    mat_max         = wafer_map_mat(max_outside_ndx,:);
    for cur_max_ndx = 1:1:cnt_max
        fprintf('\n%-4d \t\t\t %-4d \t\t\t %-4d  \t\t\t %-7.4f',mat_max(cur_max_ndx,1),mat_max(cur_max_ndx,2),mat_max(cur_max_ndx,3),mat_max(cur_max_ndx,4));
    end
    fprintf('\n');
end % 

if (wanted_contour_delta_fsr)
    % USER SPECIFIED DELTA FSR CONTOUR LINES
    % old way: detla_fsr        = (max_fsr - min_fsr)/wanted_contour_values_cnt_fsr;
    contour_values              = fsr_heat_min:wanted_contour_delta_fsr:fsr_heat_max;
    ploted_contour_values_cnt   = size(contour_values,2);
    contour_values              = round(contour_values,3);  % force 3 decimal for contour lines. Per Aaron request.
    [contour_mat,contour_handle]    = contourf(contour_data,contour_values,'ShowText','on','Clipping','off'); % ***** REAL CONTOUR PLOT ******
else
    % PRODUCE A FIX NUMBER OF CONTOUR LINES.
    [contour_mat,contour_handle]    = contourf(contour_data,ploted_contour_values_cnt,'ShowText','on','Clipping','off'); % ***** REAL CONTOUR PLOT ******
end

hold all;
fprintf('\n');
% Produce list of die BELOW:
ndx_cur_level = (wafer_map_mat(:,4) < contour_handle.LevelList(1));
fprintf('FSR Range BELOW: %-7.3f             Die: %-4d ', contour_handle.LevelList(1));
cur_die_list = wafer_map_mat(ndx_cur_level,1);
for cur_die_val =1:1:size(cur_die_list,1)
    fprintf(' %-4d ', cur_die_list(cur_die_val));
end
fprintf('\n');
fprintf('\n');
% Produce list of die on each level:
for cur_level = 1:1:size(contour_handle.LevelList,2)-1
    ndx_cur_level = (wafer_map_mat(:,4) >= contour_handle.LevelList(cur_level) ) & (wafer_map_mat(:,4) < contour_handle.LevelList(cur_level+1));
    fprintf('FSR Range FROM:  %-7.3f TO %-7.3f  Die: %-4d ', contour_handle.LevelList(cur_level),contour_handle.LevelList(cur_level+1));
    cur_die_list = wafer_map_mat(ndx_cur_level,1);
    for cur_die_val =1:1:size(cur_die_list,1)
        fprintf(' %-4d ', cur_die_list(cur_die_val));
    end
    fprintf('\n');
end

fprintf('\n');
% Produce list of die ABOVE:
ndx_cur_level = (wafer_map_mat(:,4) >=contour_handle.LevelList(end));
fprintf('FSR Range ABOVE: %-7.3f             Die: %-4d ', contour_handle.LevelList(end));
cur_die_list = wafer_map_mat(ndx_cur_level,1);
for cur_die_val =1:1:size(cur_die_list,1)
    fprintf(' %-4d ', cur_die_list(cur_die_val));
end
fprintf('\n');
%grid on not yet. Later.
contour_axes    = gca;
colorbar(contour_axes);
contour_x       = get(contour_axes,'XAxis');
limits_contour  = contour_x.Limits;
x_min           = limits_contour(1);
x_max           = limits_contour(2);
grid minor
grid on;
contour_axes.XAxis.MinorTick        = 'on';
contour_axes.XAxis.MinorTickValues  = x_min:1:x_max;

contour_y       = get(contour_axes,'YAxis');
limits_contour  = contour_y.Limits;
y_min           = limits_contour(1);
y_max           = limits_contour(2);

contour_axes.YAxis.MinorTick = 'on';
contour_axes.YAxis.MinorTickValues = y_min:1:y_max;
drawnow;
grid minor
grid on;

set(contour_axes,'FontSize',9);
str_title       = strcat('WAFER MAP -_', plot_title);
str_title       = strcat(str_title,'  _',sprintf('-  FSR Range: %-7.3f to _%-7.3f _(%-4.3f)',min_fsr,max_fsr,max_fsr-min_fsr));
title(contour_axes,strrep(str_title,'_',' '),'FontSize',10);
ylabel(contour_axes, xlabel_plot);
xlabel(contour_axes, ylabel_plot);
zlabel(contour_axes, zlabel_plot);
set(contour_axes,'XGrid','on');
set(contour_axes,'YGrid','on');
grid minor

% Plot legend: Use only found bins:
% done in process wafers.

%set(gca,'Ydir','reverse')
%hold off;

% wanted_contour_values_cnt_fsr  = 5;
% if (wanted_contour_values_cnt_fsr)
%     % User specified a number of contour lines:    
%     detla_fsr            = (max_fsr - min_fsr)/wanted_contour_values_cnt_fsr;
%     contour_values      = min_fsr:detla_fsr:max_fsr;
%     %contour_values = [0,10,20]
%     clabelm(contour_mat,contour_handle,contour_values);
% end

end % fn wm_plot_contour
% jas_tbd:  1. title: lot.   display leyend with ranges. fix minor grid.   
% from runcard:    
% part_bins:   
% 1 Available Spotting Chip     white   black text
% 2 Test Chips (QC)             orange  white text ... 
% 3 Future Test 1               blue    
% 4 Future Test 2               green 
% 5 Edge Die                    Black    white text
% 6 Excluded Chips (Fail FSR)   Red     (Fail FSR) Optical test Station
% fix: lot number: missing the P and the last digit (3)                  DONE
            


