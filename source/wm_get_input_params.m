function [ input_params_struct,error_message ] = wm_get_input_params( )
%GET_WM_INPUT_PARAMS Retrieves input params for the sampling plan
%   Returns a struct with the params. 
%   input files: hardcoded files: .m or .xlsm sitting in the ..\configure\ directory.

error_message                   = '';
%cfg_params_config_path         = 'W:\wafer_map\';                          % End User to be copied to ..\cfg_and_data\
%cfg_params_config_path          = 'C:\Users\jsanchez\Documents\wafer_map\configure\'; % Jas's  file for dbg
%wafer_map_all_die_dir      = 'W:\wafer_map\wafer_map.xlsm';                                 % Nick's file for production
%wafer_map_all_die_dir      = 'C:\Users\jsanchez\Documents\wafer_map\configure\wafer_map_master.xlsm';        % Jas's  file for dbg

logical_str                     = {'FALSE', 'TRUE'};
wafer_map_root_dir              = 'wafer_map';         % jas_hardcoded_expected  C:\Users\jsanchez\Documents\dcrc\source\cfg\wafer_map_cfg.m
if (isdeployed)
    % COMPILED VERSION USED A .xlsm FILE FOR CONFIGURATION
    cfg_params_file_name        = 'wafer_map_configure.xlsm'; % jas_hardcoded_expected in this dir: ..\cfg\ relative to the exe
else
    % SCRIPT VERSION USED A .m FILE FOR CONFIGURATION
    cfg_params_file_name        = 'wafer_map_configure.xlsm';     
    %cfg_params_file_name        = 'wafer_map_configure.m'; % jas_hardcoded_expected in this dir: ..\cfg\ relative to the source
end
cur_pwd                         = pwd;
input_params_struct             = struct;
ndx                             = strfind(cur_pwd,wafer_map_root_dir);
if ( (~(isempty(ndx)))  && (ndx > 1) )
    % FOUND MAIN DIR FOR THE APP: wafer_map
    base_dir                    = cur_pwd(1:ndx+8);
    cfg_dir                     = strcat(base_dir,'\configure\');
    
    % Check if the cfg folder exists:
    if ( ( exist(cfg_dir,'dir') == 7))
        % Folder Exist and it is a directory: We are almost done.
        cfg_params_full_name =strcat(cfg_dir,cfg_params_file_name);
    else
        % Folder does not exist: Prompt for loc of input excel file.
        error_message = sprintf('ERROR: wafer_map can not run due to missing: \nconfigure directory: %s\n',cfg_dir);
        return
    end
    % CG DIRECTORY EXIST Try to pen cfg file
    
    if ((isdeployed) || ( strcmp(cfg_params_full_name(end-3:end),'xlsm')))
        % COMPILED VERSION USED A .xlsm FILE FOR CONFIGURATION: read it.
        excel_tab_number            = 1;
        [input_params_struct,error_message] =  wm_get_excel_input_params( cfg_params_full_name,excel_tab_number);
        if ((~isempty(error_message)) && (strcmpi(error_message(1:5),'ERROR')))
            return;
        end
        if ( (~isempty(input_params_struct)) &&  ((isempty(error_message)) || (strcmp(error_message(1:7),'WARNING')) ) )
            fprintf('\n Reading CONFIG PARAMETERS ... DONE ');
            return
        else
            fprintf('\n Reading CONFIG PARAMETERS ... ERROR\n');
            fprintf('%s',error_message);
        end
        return;
    end; % deployed
    % SCRIPT VERSION USED A .m FILE FOR CONFIGURATION
    try
        
        % SCRIPT VERSION. RUNNING INSIDE MATLAB: read the text file
        fprintf('\n\n Reading Configuration file: \t\t\t\t\t   %s\n',cfg_params_full_name);
        % the function run can not be compiled
        run(cfg_params_full_name)
    catch
        error_message = sprintf('ERROR: wafer_map can not run due to missing: \ncfg file: %s',cfg_params_file_path);
        return;
    end
    
    % Pack all the input parameters into a struct to be returned.
    % input_params_struct struct: __________________________________________________
    field_01 = 'wafer_map_all_die_dir';             value_01 = { wafer_map_all_die_dir          };
    field_02 = 'wafer_map_all_die_file';            value_02 = { wafer_map_all_die_file         };
    field_03 = 'fsr_heat_min';                  	value_03 = { str2double(fsr_heat_min)       };
    field_04 = 'fsr_heat_max';                      value_04 = { str2double(fsr_heat_max)       };
    field_05 = 'webservice_url';                    value_05 = { webservice_url 	            };

    field_06 = 'wanted_contour_delta_fsr';         value_06 = { str2double(wanted_contour_delta_fsr  )};
    field_07 = 'wanted_contour_values_fsr_cnt';    value_07 = { str2double(wanted_contour_values_fsr_cnt)};
%     field_08 = 'bin_number_to';                      value_08 = { bin_number_to               };
%     field_10 = 'part_number';                        value_10 = { part_number                 };
%     field_11 = 'wm_ok_do_partially_flag';            value_11 = { wm_ok_do_partially_flag 	};    
%     field_12 = 'replace_chip_if_miss_flag';          value_12 = { replace_chip_if_miss_flag   };
%     field_13 = 'check_availab_only_flag';            value_13 = { check_availab_only_flag  	};
    input_params_struct = struct(...
        field_01 , value_01, field_02, value_02, field_03, value_03   ...
        ,field_04 ,value_04, field_05, value_05 ...
         , field_06, value_06  ,field_07 ,value_07 );
%         field_08, value_08, field_09, value_09   ...
%         ,field_10 ,value_10, field_11 ,value_11                       ...
%         ,field_12 ,value_12, field_13 ,value_13);
    % Diwmlay arguments:
    fprintf('\n Input Parameters:                               ');
    fprintf('\n 1 - Wafer Map File Name: All Die                   %-s',wafer_map_all_die_dir);  % 1
    fprintf('\n 2 - Wafer Map File Name: QC  Die                   %-s',wafer_map_all_die_file);   % 2   
    fprintf('\n 3 - FSR heat map mininum value:                    %-s',fsr_heat_min);                   % 3
    fprintf('\n 4 - FSR heat map maximum value:                    %-s',fsr_heat_max);                   % 4
    fprintf('\n 5 - webservice_url                                 %-s',webservice_url);                 % 5   bin 1 by default
    fprintf('\n 6 - Contour Delta FRS value:                       %-s',wanted_contour_delta_fsr);                   % 4
    fprintf('\n 7 - Contour Values FSR count                       %-s',wanted_contour_values_fsr_cnt);                 % 5   bin 1 by default

%     fprintf('\n 2 - Work Order number:                             %s',wor_order_number);             % 2    
%     fprintf('\n 6 - Op Code Previous Step:                        %s',fsr_heat_max_previous_step);        % 6
%     fprintf('\n 7 - Bin Number From: (source)                     %s',bin_number_from);              % 7   bin 1 by default
%     fprintf('\n 8 - Bin Number to:   (destination)                %s',bin_number_to);                % 8   wm + 1 by default
%     fprintf('\n10 - part_number                                   %s',part_number);                  %10   wm + 1 by default

%     fprintf('\n11 - wafer_map_ok_do_partial          FLAG     %s',logical_str{wm_ok_do_partially_flag+1});      % 12 This will enable to do partial sampling plans.                                                                                                          %   If set to false: it will not do the sampling plan, unless it can be done completelly
%     fprintf('\n12 - Replace chip if is missing           FLAG     %s',logical_str{replace_chip_if_miss_flag+1});    % 13 
%     fprintf('\n13 - Check Availability Only (just check) FLAG     %s',logical_str{check_availab_only_flag+1});      % 11 Recomended to see what is going to happening, without doing ANYTHING.
    fprintf('\n');
else
    error_message = sprintf('ERROR: wafer_map APP can not run due to missing: \nroot directory: %s\nSee read_me.txt file in the doc directory\n',wafer_map_root_dir);
end % dir exist.

end % fn: get_wm_input_params

% %             fid = fopen(cfg_params_file_path);
% %             [params_read] = textscan(fid,'%s%s','delimiter','=');
% %             param_names  = params_read{1};
% %             param_values = params_read{2};
% %             fid = fclose(fid);
% %             %sprintf('\n\AFTER Config file =                     %s\n',cfg_params_file_path);
% %             for cur_param_ndx =1:1:size(param_names,1)
% %                 % OK: Get rid of the quotes
% %                 if (strcmp(param_names{cur_param_ndx},'request_date_str'))
% %                     request_date_str = strrep(param_values{cur_param_ndx},'''','');
% %                 end
% %                 if (strcmp(param_names{cur_param_ndx},'path_xml_files'))
% %                     path_xml_files = strrep(param_values{cur_param_ndx},'''','');
% %                 end
% %                 if (strcmp(param_names{cur_param_ndx},'exper_name'))
% %                     % get rid of the quotes
% %                     exper_name = strrep(param_values{cur_param_ndx},'''','');
% %                 end
% %             end
