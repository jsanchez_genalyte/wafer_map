function [ wafer_map_mat,error_message ] = wm_runcard_get_bins_for_wafer(wafer_lot_number_str,wafer_number,wafer_map_mat,input_params)
%WM_RUNCARD_GET_BINS_FOR_WAFER gets from runcard the bins for a given wafer lot and wafer number.
%  Output: Adds a column to the wm_runcard_get_bins_for_wafer with the bin numbers.
%                                  P162143.23.0010
% set up loop invariants:
part_number                  = '29005';  % Alex: part_number = 'EXP_CHIP'; %  Mario: '29005'
webservice_url               = input_params.webservice_url;
%logical_str                 = {'FALSE', 'TRUE'};
% Connect to run card:

fprintf('\nRetrieving Bin data from RUNCARD. Please wait ~ 2 minutes per file .....\n');
createClassFromWsdl(webservice_url);
obj                        	= runcard_wsdl;                                                   %
% Real Processing:
wafer_die_cnt               = size(wafer_map_mat,1);
wafer_bins                  = nan(wafer_die_cnt,1);
wafer_number_str            = sprintf('%2d',wafer_number);
wafer_number_str            = strrep(wafer_number_str,' ','0');
die_serial_part_1           = strcat('P',wafer_lot_number_str,'.',wafer_number_str,'.');

die_bad_data_cnt =0;
for cur_die_ndx =1:1:wafer_die_cnt
    % Fetch inventory for each serial:
    die_str                 = sprintf('%4d',wafer_map_mat(cur_die_ndx,1));  %  col 1 is the die number
    die_str                 = strrep(die_str,' ','0');                                % replace blanks with zeros.
    wanted_serial           = strcat(die_serial_part_1,die_str);
    [resp_serial, error_value, error_message]   = fetchInventoryItems(obj, wanted_serial,'',part_number, '', '', '' ,  '', '', '');  % op_code=A
    if (~(isempty(error_message)))
        error_message =  strcat( error_message, '\nRuncard fetchInventoryItems Error: = ',num2str(error_value), ' for lot: ',wafer_lot_number_str, ' and wafer ',num2str(wafer_number));
        return;
    end;
    % HAVE THE SERIAL STRUCT FROM RUNCARD: Store the bin
    if (~(isempty(resp_serial)))
        wafer_bins(cur_die_ndx) = str2double(resp_serial.part_bin);
    else
        die_bad_data_cnt = die_bad_data_cnt+1;
        %fprintf('\nEmpty runcard response for die: %s\n', die_str);
    end
end % for each die in the wafer
if (die_bad_data_cnt)
    fprintf('\nEmpty runcard response total cnt = : %-4d\n', die_bad_data_cnt);
end


if (cur_die_ndx == wafer_die_cnt)
    fprintf('\n\n\t LOT:  \t\t%-s   \n\tWAFER: %-s COMPLETE RUNCARD DATA.',wafer_lot_number_str,num2str(wafer_number));
    wafer_map_mat = [ wafer_map_mat wafer_bins];
end
fprintf('\n');

end % fn sp_part_bin_update