function [ output_args ] = fprintf_log( format_string,varargin )
%fprintf_log prints to command window or to a log file
%   hard coded log filename  

log_to_file_flag = 1;
if (log_to_file_flag)
	file_name_log = 'dcrc_gen_xml_log.txt';
	file_id = fopen(file_name_log,'a');
end

try
if (isdeployed ||  log_to_file_flag)
% WRITING TO A LOG FILE
	fprintf(file_id,format_string,varargin);
	
else
% WRITING TO CMD WINDOW
	fprintf(format_string,varargin);
end
catch
	if (isdeployed ||  log_to_file_flag)
	    fprintf('\nERROR: Exception thrown while trying to print to log file %s\n',file_name_log);
		format_string
		varargin
		else
	    fprintf('\nERROR: Exception thrown while trying to print to cmd window \n');	
		format_string
		varargin		
		end
end
}