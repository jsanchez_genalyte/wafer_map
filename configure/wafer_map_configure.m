% % % % file:              sampling_plan_configure.m
% % % % ref:               configuration file for the sampling_plan.exe app. used only from MATLAB ignored when deployed
% % % % Instructions:      Modify this file with the parameters to run this app.
% % % % Expected location: ..\configure\sampling_plan_configure.m   relative to the executable.
% % % 
% % % % Expected dir structure:
% % % %   c:\username\Documents\sampling_plan      main dir.
% % % %     source
% % % %     exe\sampling_plan.exe
% % % %     configure\sampling_plan_configure.m   (this file)
% % % %     configure\sampling_plan_configure.xls (for deployment: production)
% % % 
% % % % pipeline:  sampling_plan (main) -->  sampling_plan_configure 
% % % 
% % % % cfg parameters: _________________________________________________________________________
disp('\n\n using .../configure/sampling_plan_configure.m file \n\n')
% % % sp_master_excel_file_name   = 'C:\Users\jsanchez\Documents\sampling_plan\configure\sampling_plan_master.xlsm';       % 1 jas_
% % % sampling_plan_number_to_use = 2; % test: 5;         % 6          % 11 and 12 for   0051                                                % 2
% % % wo_number                   = 'DWO-0051'; % 'DWO-0058'; % 'DWO-0051'; % DWO-0052' ; % 'DWO-0058'; % 'DWO-0021';     % 3
% % % seq_num                     = '40';                                                     % 4
% % % op_code                     = 'D320';     %'S380';                                      % 5
% % % op_code_previous_step       = 'A230';     %'S380';                                      % 6
% % % 
% % % bin_number_from             = '1';                                                      % 7
% % % bin_number_to               = '2';                                                      % 8
% % % 
% % % sp_ok_do_partially_flag     = true; % DEFAULT == false.                                 % 9
% % % %                                    % true  == Perform partial sampling plans and report which part was done and which was not.
% % % %                                    false == Move the chips to the proper bin: ONLY IFF the sp can be done completely
% % % replace_chip_if_miss_flag   = true;  % DEFAULT == false.                                % 10   
% % % %                                    % true  == attempt to find nearest neightboor for missing chips
% % % %                                    % false == Do not attempt to do replacements (see sp_ok_do_partially_flag)
% % % check_availab_only_flag     = true;  % DEFAULT = true.                                  % 11
% % % % true  == Just check if it is possible to do the sp. Do not change any bins.
% % % % false == Attempt to move the chips to the proper bin. See next flag.
% % % 
% % % % flags: 1_ sp_ok_do_partially_flag 
% % % %        2_ replace_if_chip_is_miss_flag 
% % % %        3_ check_availab_only_flag
