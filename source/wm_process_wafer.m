function [ ] = wm_process_wafer( wafer_set_data_cell,input_params )
%WM_PROCESS_WAFER process and plot a wafer_map_mat
%   Detailed explanation goes here

% S1. Setup
%wm_wanted_header   = {'INDEX'	'ROW'	'COLUMN' 'FSR'};
%col_ndx_die = 1;

wafer_count         = size(wafer_set_data_cell,1);

for cur_wafer_ndx = 1:1:wafer_count
    %wafer_map_all_die_file      = wafer_set_data_cell(cur_wafer_ndx).file_name
    wafer_set_data_struct        = wafer_set_data_cell(cur_wafer_ndx);
    wafer_number                 = wafer_set_data_struct{1}.wafer_number;
    wafer_lot_number_str         = wafer_set_data_struct{1}.wafer_lot_number_str;
    wafer_map_mat                = wafer_set_data_struct{1}.wafer_map_all_mat;
    %                                 % Chungs excel file cols.excel deppendency jas_
    col_ndx_die                  = 1; % This is the die index. excel deppendency jas_
    col_ndx_row                  = 2; % This is the die row.   excel deppendency jas_ y
    col_ndx_col                  = 3; % This is the die col.   excel deppendency jas_ x
    col_ndx_fsr                  = 4; % This is the die fsr.   excel deppendency jas_
    
    % S2. Fill in wafer_map_mat
    % % die_all_cnt =0;
    % % for cur_wafer_ndx = 1:1:size(wafer_map_mat,2)
    % %     die_all_cnt = die_all_cnt+ size(wafer_map_mat(cur_wafer_ndx).data_mat,1);
    % % end
    %die_all_cnt             = size(wafer_map_mat,1);
    min_wafer               = min(wafer_map_mat);
    max_wafer               = max(wafer_map_mat);
    
    % wafer_map_mat = nan(die_all_cnt,4);
    % for cur_wafer_ndx = 1:1:size(wafer_map_mat,2)
    %     wafer_map_mat(1:size(wafer_map_mat(cur_wafer_ndx).data_mat(:,4)),1:4) =    wafer_map_mat(cur_wafer_ndx).data_mat(:,1:4);
    % end
    
    fprintf( '\n\t\t\t\tINDEX\t\t\t ROW    \t\tCOLUMN   \tFSR');
    fprintf('\nMIN VALUES\t\t%-4d\t\t\t %-4d\t\t\t%-4d\t\t %-6.2f\t\t ',min_wafer(1),min_wafer(2),min_wafer(3),min_wafer(4));
    fprintf('\nMAX VALUES\t\t%-4d\t\t\t %-4d\t\t\t%-4d\t\t %-6.2f\t\t ',max_wafer(1),max_wafer(2),max_wafer(3),max_wafer(4));
    fprintf('\n');
    

    
    % get bins for current wafer
    % [ ] wm_runcard_get_bins_for_wafer( strcat('P',wafer_lot_number_str,num2str(wafer_number)),wafer_map_mat); %  num2str(wafer_map_mat(col_die,cur_wafer))); % ,input_params )
    [ wafer_map_mat2,error_message ] = wm_runcard_get_bins_for_wafer(wafer_lot_number_str,wafer_number,wafer_map_mat,input_params);
    if (~(isempty(error_message)))
        return;
    end
    
    % S3. Set up Plot for  wafer_map_mat
    die_all_run_card_cnt                = size(wafer_map_mat2,1);
    plot_data_info_struct.figure_title  = sprintf('MAP FOR WAFER LOT: %-s ',wafer_lot_number_str);
    plot_data_info_struct.plot_title    = sprintf('LOT %-s  - NUMBER: %-4d  -  DIE COUNT: %-4d',wafer_lot_number_str,wafer_number,die_all_run_card_cnt);
    plot_data_info_struct.xlabel_plot   = ' DIE  COLUMN ';
    plot_data_info_struct.ylabel_plot   = ' DIE  ROW ';
    plot_data_info_struct.zlabel_plot   = ' FSR ';
    
        
    %D1. Draw the contour.
    wm_plot_contour( plot_data_info_struct,wafer_map_mat,input_params,0);  % 1 == delete_old_figures.% *********************************** %
    fprintf('\nCalculating Labels and Ranges. Please wait ~ 1 minute per file .....\n');
    
    % BUILD THE LEYEND: from runcard:  part_bins: names and colors: Let's match runcard.
    text_legend_all_cel = {
        'Available Spotting Chip_' ...   % 1 white   black text
        ;'Test Chips (QC)          _' ...  % 2  orange  white text ...
        ;'Future Test      1            _' ...  % 3 blue     white text ...
        ;'Future Test      2            _' ...  % 4 green    white text ...
        ;'Edge Die                  _' ...  % 5 Black    white text
        ;'Excluded Chips (Fail FSR)_' ...  % 6 Red     (Fail FSR) Optical test Station
        ;'Unmeasured FSR        _'};    % 7 Grey    white text ...
    text_legend_plot_cel = cell(size(text_legend_all_cel));
    % bin colors/order/shape:    1              2              3             4             5             6          7 (unknown) used as buffer.
    color_text_code    = { rgb('Black')   rgb('Orange')  rgb('Blue')   rgb('Green')  rgb('Black')  rgb('Red')   rgb('Grey') };
    color_shape_code    = { rgb('White')   rgb('Orange')  rgb('Blue')   rgb('Green')  rgb('Black')  rgb('Red')   rgb('Grey') };
    color_border_code   = { rgb('White')   rgb('Orange')  rgb('Blue')   rgb('Green')  rgb('Black')  rgb('Red')   rgb('Grey') };
    shape_code          = {     's'            's'            's'           's'           's'           's'          's'     };
    total_cnt_bin       = 7;       %jas_ runcard deppendency. If runcard adds bins, I will have to add bins here too.
    % Plot each die with a given bin:
    bin_ax              = gca;
    bin_ax.YLim(2)      = bin_ax.YLim(2)+1; % minor tweack to fix top of the plot (stepping into tile)
    bin_ax.XLim(2)      = bin_ax.XLim(2)+1; % minor tweack to fix rig of the plot right border.
    contour_x           = get(bin_ax,'XAxis');
    limits_contour      = contour_x.Limits;
    x_min               = limits_contour(1);
    x_max               = limits_contour(2);
    
    contour_y           = get(bin_ax,'YAxis');
    limits_contour      = contour_y.Limits;
    y_min               = limits_contour(1);
    y_max               = limits_contour(2);
    
    
    cnt_squares         = x_max-x_min-1;
    max_bin             = max(wafer_map_mat2(:,end));
    
    % Check that there are not too many bins.
    if (max_bin > total_cnt_bin  )
        fprintf('ERROR: Found %-4d bins in RunCard and the WAFER_MAP app currently handles only up to %-4d bins',max_bin,total_cnt_bin);
        fprintf('ERROR: Displaying wafer maps for only the first %-d bins',total_cnt_bin);
        % Reset max_bin to only what this app can handle so far:
        max_bin = total_cnt_bin;
    end
    bin_count_list      = zeros(total_cnt_bin,1);
    
    % D2. Draw the 2 SQUARES (2 per box to account for rectangular boxes) per bin.
    % Do not draw bin 1: In white: This is what it is available.
    % future: Interactive: Select rectangles and accumulate selection.
    % right click Accum selection .... Right Click list selected die.(text)
    
    for cur_bin = 1:1:max_bin
        % plot one bin at a time. Skip bin 1.  use scatter of 2 blocks.
        cur_bin_ndx                         = wafer_map_mat2(:,end) == cur_bin;
        cur_bin_cnt                         = sum(cur_bin_ndx);
        if (cur_bin == 1)
            text_legend_plot_cel{cur_bin}   = sprintf(' %-25s%-4d die',text_legend_all_cel{cur_bin}, cur_bin_cnt);
        else
            text_legend_plot_cel{cur_bin}   = sprintf(' %-29s %-4d die',text_legend_all_cel{cur_bin}, cur_bin_cnt);
        end
        if (cur_bin == 7)
            text_legend_plot_cel{cur_bin}   = sprintf(' %-25s%-4d die',text_legend_all_cel{cur_bin}, cur_bin_cnt);
        end
        if (cur_bin_cnt)
            % THIS BIN IS PRESENT IN THE DATA: Store the total and build the legend to plot
            bin_count_list(cur_bin)     = cur_bin_cnt;
            if (cur_bin == 1)
                % NO BOXES FOR BIN 1 i.e the AVAILABLE.
                continue;
            end
            
            wafer_map_mat_bin           = wafer_map_mat2(cur_bin_ndx,col_ndx_row:col_ndx_col); % 2:x, 3:y
            x_temp                      = wafer_map_mat_bin(:,2) - 0.3; % shift the x of the 1st square  a bit to the left
            y_temp                      = wafer_map_mat_bin(:,1);
            scatter(bin_ax,x_temp,y_temp...
                ,'LineWidth'        ,0.2...
                ,'MarkerFaceColor'  ,color_shape_code{ cur_bin}...
                ,'MarkerEdgeColor'  ,color_border_code{cur_bin}...
                ,'Marker'           ,shape_code      { cur_bin}...
                ,'SizeData',340); % ,'MarkerFaceColor',dark_green_color);
            x_temp                      = x_temp + 0.45;             % shift the x of the 2nd square  a bit to the ritght
            y_temp                      = wafer_map_mat_bin(:,1);
            scatter(bin_ax,x_temp,y_temp...
                ,'LineWidth'        ,0.2...
                ,'MarkerFaceColor'  ,color_shape_code{ cur_bin}...
                ,'MarkerEdgeColor'  ,color_border_code{cur_bin}...
                ,'Marker'           ,shape_code      { cur_bin}...
                ,'SizeData',340); % ,'MarkerFaceColor',dark_green_color);
        end % bin is present;
    end % for each bin
    
    % D3. Draw the white grid with die_indices: Independient of the bin:
    %     Using matlab function downloaded from the mathworks. 
    
    for cur_die_ndx = 1:1:size(wafer_map_mat2,1)
        labelpoints( wafer_map_mat2(cur_die_ndx,col_ndx_col), wafer_map_mat2(cur_die_ndx,col_ndx_row),wafer_map_mat2(cur_die_ndx,col_ndx_die),'position','C','FontSize', 7,'Color', rgb('White'));
    end  
    
    % D4. Draw on the top right corner and top left the labels using it's own color for the text
    cur_y_label = y_max - 0.5;
    cur_x_label = 1.2;
    cur_label_plot_cnt=0;
    for cur_bin = 1:1:max_bin
        % plot one legend at a time. Skip bin 1.  use scatter of 2 blocks.
        if (bin_count_list(cur_bin))
            % THIS BIN IS PRESENT IN THE DATA: draw the legend
            cur_label_plot_cnt = cur_label_plot_cnt+1;
            text(cur_x_label, cur_y_label, strrep(text_legend_plot_cel{cur_bin},'_','')...
                ,'HorizontalAlignment','right','FontSize',10,'color',color_text_code{cur_bin});
            cur_y_label = cur_y_label - 1; % function of font size and window size jas_tbd.
            if (cur_label_plot_cnt ==3)
                cur_y_label =  2.5;   % reset it max_y -1;
                cur_x_label = 1.2;
            end
        end 
    set(gca,'Xdir','reverse','Ydir','reverse')
    
    % noi fig.InvertHardcopy = 'off';
    % Save file with figure.
    wafer_number_str   = sprintf('%2d',wafer_number);
    wafer_number_str   = strrep(wafer_number_str,' ','0');
    fig_name            = strrep(strcat(input_params.wafer_map_figure_output_dir,'\wafer_figure_',wafer_lot_number_str,'_',wafer_number_str,'.pdf'),' ','');
    saveas(gcf,fig_name)
    
    hold off;
    
end % for current wafer

end % fn wm_process_wafer

% ,'HorizontalAlignment','right'

% % %
% % %     % S4.Fill contour map
% % %     contour_data    = nan(max_wafer(col_ndx_row),max_wafer(col_ndx_col));
% % %     for cur_die_ndx = 1:1:die_all_cnt
% % %         cur_row     = wafer_map_mat(cur_die_ndx,col_ndx_row);
% % %         cur_col     = wafer_map_mat(cur_die_ndx,col_ndx_col);
% % %         cur_fsr     = wafer_map_mat(cur_die_ndx,col_ndx_fsr);
% % %         if (isfinite(cur_row) && isfinite(cur_col) && isfinite(cur_fsr) )
% % %             contour_data(cur_row  ,cur_col )=  cur_fsr;
% % %         end
% % %     end

